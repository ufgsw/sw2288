/* 
 * File:   lis3mdl.h
 * Author: Diego
 *
 * Created on July 23, 2015, 4:45 PM
 */
#include "project.h"

//#define csmagn_on()   Cy_GPIO_Write( cs_magnetometro_PORT, cs_magnetometro_NUM, 0 );
//#define csmagn_off()  Cy_GPIO_Write( cs_magnetometro_PORT, cs_magnetometro_NUM, 1 );

#define LIS3MDL_OK   (lis3mdl.who_am_i == 0x3d)

enum eRegisters
{
    OFFSET_X_REG_L_M  = 0x05,
    OFFSET_X_REG_H_M  = 0x06,       
    OFFSET_Y_REG_L_M  = 0x07,
    OFFSET_Y_REG_H_M  = 0x08,       
    OFFSET_Z_REG_L_M  = 0x09,
    OFFSET_Z_REG_H_M  = 0x0A,       
    WHO_AM_I          = 0x0f,
    CTRL_REG1         = 0x20,
    CTRL_REG2         = 0x21,
    CTRL_REG3         = 0x22,
    CTRL_REG4         = 0x23,
    CTRL_REG5         = 0x24,
    STATUS_REG        = 0x27,
    OUT_X_L           = 0x28,
    OUT_X_H           = 0x29,
    OUT_Y_L           = 0x2a,
    OUT_Y_H           = 0x2b,
    OUT_Z_L           = 0x2c,
    OUT_Z_H           = 0x2d,
    TEMP_OUT_L        = 0x2e,
    TEMP_OUT_H        = 0x2f,
    INT_CFG           = 0x30,
    INT_SRC           = 0x31,
    INT_THS_L         = 0x32,
    INT_THS_H         = 0x33       
};

enum eStatus
{
  XDA   = 0x01,
  YDA   = 0x02,
  ZDA   = 0x04,
  ZYXDA = 0x08,
  XOR   = 0x10,
  YOR   = 0x20,
  ZOR   = 0x40,
  ZYXOR = 0x80     
};

enum eReg1
{
  ST        = 0x01,
  FAST_ODR  = 0x02,
  DO0       = 0x04,
  DO1       = 0x08,
  DO2       = 0x10,
  OM0       = 0x20,
  OM1       = 0x40,
  TEMP_EN   = 0x80
};

enum eReg2
{
  SOFT_RST  = 0x04,
  REBOOT    = 0x08,
  FS0       = 0x20,
  FS1       = 0x40
};
  
enum eReg3
{
  MD0   = 0x01,
  MD1   = 0x02,
  SIM   = 0x04,
  LP    = 0x20
};

enum eReg4
{
  BLEE  = 0x02,
  OMZ0  = 0x04,
  OMZ1  = 0x08,  
};

enum eReg5
{
  BDU       = 0x40,
  FAST_READ = 0x80
};

enum eIntCfg
{
   IEN  = 0x01,
   LIR  = 0x02,
   IEA  = 0x04,
   ZIEN = 0x20,
   YIEN = 0x40,
   XIEN = 0x80
};

typedef struct
{
  unsigned char who_am_i;
  unsigned char status_reg;
  unsigned char idle;
  
  unsigned char reg1;
  unsigned char reg2;
  unsigned char reg3;
  unsigned char reg4;
  unsigned char reg5;
  
  int16_t value_x;
  int16_t value_y;
  int16_t value_z;
  
  int16_t offset_x;
  int16_t offset_y;
  int16_t offset_z;
  
  int16_t temperatura;
  
}sLis3mdl;
extern sLis3mdl lis3mdl;

bool Lis3mdl_Init();
bool Lis3mdl_ReadValues();
void Lis3mdl_Write( unsigned char reg, unsigned char data);
void Lis3mdl_Idle();
void Lis3mdl_StartContinuous();
void Lis3mdl_StartSingle();
void Lis3mdl_ReadStatusReg();


