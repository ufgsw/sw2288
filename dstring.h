/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef DSTRING_H
#define DSTRING_H
    
#include "error.h"    

#define MAX_CAMPI       20
#define MAX_LEN_CAMPO   20
#define SEPARATORE ';'

#define campo(x) (const char*)&campi.var[x][0]
    
//#define campo1  (const char*)&campi.st[0][0]
//#define campo2  (const char*)&campi.st[1][0]
//#define campo3  (const char*)&campi.st[2][0]
//#define campo4  (const char*)&campi.st[3][0]
//#define campo5  (const char*)&campi.st[4][0]
//#define campo6  (const char*)&campi.st[5][0]
//#define campo7  (const char*)&campi.st[6][0]
//#define campo8  (const char*)&campi.st[7][0]
//#define campo9  (const char*)&campi.st[8][0]
//#define campo10 (const char*)&campi.st[9][0]
// 
//#define campo11 (const char*)&campi.st[10][0]
//#define campo12 (const char*)&campi.st[11][0]
//#define campo13 (const char*)&campi.st[12][0]
//#define campo14 (const char*)&campi.st[13][0]
//#define campo15 (const char*)&campi.st[14][0]
//#define campo16 (const char*)&campi.st[15][0]
//#define campo17 (const char*)&campi.st[16][0]
//#define campo18 (const char*)&campi.st[17][0]
//#define campo19 (const char*)&campi.st[18][0]
//#define campo20 (const char*)&campi.st[19][0]
 
 
typedef struct
{
  char var[MAX_CAMPI][MAX_LEN_CAMPO];
  int  len;
}parse_s;
extern parse_s campi;

eErrorCode parseString( char* st);

extern parse_s campi;

#endif

/* [] END OF FILE */
