/*******************************************************************************
* File Name: Comp_1.c
* Version 1.0
*
* Description:
*  This file provides the source code to the API for the Comp_1
*  component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/
#include "Comp_1.h"

uint8_t Comp_1_initVar = 0u;

/* Initialize compPower to the customizer selection. */
cy_en_ctb_power_t Comp_1_compPower = CY_CTB_POWER_MEDIUM;

const cy_stc_ctb_opamp_config_t Comp_1_compConfig =
{
    .deepSleep      = CY_CTB_DEEPSLEEP_DISABLE,
    .oaPower        = CY_CTB_POWER_MEDIUM,
    .oaMode         = CY_CTB_MODE_COMP,
    .oaPump         = Comp_1_COMP_CHARGEPUMP,
    .oaCompEdge     = CY_CTB_COMP_EDGE_RISING,
    .oaCompLevel    = CY_CTB_COMP_DSI_TRIGGER_OUT_LEVEL,
    .oaCompBypass   = CY_CTB_COMP_BYPASS_SYNC,
    .oaCompHyst     = CY_CTB_COMP_HYST_10MV,
    .oaCompIntrEn   = true,
};

/*******************************************************************************
* Function Name: Comp_1_Start
****************************************************************************//**
*
* Initialize the Comp_1 with default customizer
* values when called the first time and enables the Comp_1.
* For subsequent calls the configuration is left unchanged and the comparator is
* just enabled.
*
* Comp_1_initVar: this global variable is used to indicate the initial
* configuration of this component. The variable is initialized to zero and set
* to 1 the first time Comp_1_Start() is called. This allows
* enabling/disabling a component without re-initialization in all subsequent
* calls to the Comp_1_Start() routine.
*
*******************************************************************************/
void Comp_1_Start(void)
{
    if(0u == Comp_1_initVar)
    {
       Comp_1_Init();         /* Initialize and turn on the hardware block. */
       Comp_1_initVar = 1u;
    }

    /* Turn on the CTB block and the comparator by setting the power level. */
    Comp_1_Enable();
}


/* [] END OF FILE */
