/***************************************************************************//**
* \file Sc2.c
* \version 2.0
*
*  This file provides the source code to the API for the SPI Component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "Sc2.h"
#include "sysint/cy_sysint.h"
#include "cyfitter_sysint.h"
#include "cyfitter_sysint_cfg.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*     Global variables
***************************************/

/** Sc2_initVar indicates whether the Sc2
*  component has been initialized. The variable is initialized to 0
*  and set to 1 the first time Sc2_Start() is called.
*  This allows  the component to restart without reinitialization
*  after the first call to the Sc2_Start() routine.
*
*  If re-initialization of the component is required, then the
*  Sc2_Init() function can be called before the
*  Sc2_Start() or Sc2_Enable() function.
*/
uint8_t Sc2_initVar = 0U;

/** The instance-specific configuration structure.
* The pointer to this structure should be passed to Cy_SCB_SPI_Init function
* to initialize component with GUI selected settings.
*/
cy_stc_scb_spi_config_t const Sc2_config =
{
    .spiMode  = CY_SCB_SPI_MASTER,
    .subMode  = CY_SCB_SPI_MOTOROLA,
    .sclkMode = CY_SCB_SPI_CPHA0_CPOL0,

    .oversample = 16UL,

    .rxDataWidth              = 8UL,
    .txDataWidth              = 8UL,
    .enableMsbFirst           = true,
    .enableInputFilter        = false,

    .enableFreeRunSclk        = false,
    .enableMisoLateSample     = false,
    .enableTransferSeperation = false,
    .ssPolarity               = ((((uint32_t) CY_SCB_SPI_ACTIVE_LOW) << Sc2_SPI_SLAVE_SELECT0) | \
                                 (((uint32_t) CY_SCB_SPI_ACTIVE_LOW) << Sc2_SPI_SLAVE_SELECT1) | \
                                 (((uint32_t) CY_SCB_SPI_ACTIVE_LOW) << Sc2_SPI_SLAVE_SELECT2) | \
                                 (((uint32_t) CY_SCB_SPI_ACTIVE_LOW) << Sc2_SPI_SLAVE_SELECT3)),

    .enableWakeFromSleep = false,

    .rxFifoTriggerLevel  = 0UL,
    .rxFifoIntEnableMask = 0x0UL,

    .txFifoTriggerLevel  = 0UL,
    .txFifoIntEnableMask = 0x0UL,

    .masterSlaveIntEnableMask = 0x0UL
};

/** The instance-specific context structure.
* It is used while the driver operation for internal configuration and
* data keeping for the SPI. The user should not modify anything in this 
* structure.
*/
cy_stc_scb_spi_context_t Sc2_context;


/*******************************************************************************
* Function Name: Sc2_Start
****************************************************************************//**
*
* Invokes Sc2_Init() and Sc2_Enable().
* Also configures interrupt if it is internal.
* After this function call the component is enabled and ready for operation.
* This is the preferred method to begin component operation.
*
* \globalvars
* \ref Sc2_initVar - used to check initial configuration,
* modified  on first function call.
*
*******************************************************************************/
void Sc2_Start(void)
{
    if (0U == Sc2_initVar)
    {
        /* Configure component */
        (void) Cy_SCB_SPI_Init(Sc2_HW, &Sc2_config, &Sc2_context);

        /* Set active slave select to line 0 */
        Cy_SCB_SPI_SetActiveSlaveSelect(Sc2_HW, Sc2_SPI_SLAVE_SELECT0);

        /* Hook interrupt service routine */
    #if defined(Sc2_SCB_IRQ__INTC_ASSIGNED)
        (void) Cy_SysInt_Init(&Sc2_SCB_IRQ_cfg, &Sc2_Interrupt);
    #endif /* (Sc2_SCB_IRQ__INTC_ASSIGNED) */

        /* Component is configured */
        Sc2_initVar = 1U;
    }

    /* Enable interrupt in NVIC */
#if defined(Sc2_SCB_IRQ__INTC_ASSIGNED)
    NVIC_EnableIRQ((IRQn_Type) Sc2_SCB_IRQ_cfg.intrSrc);
#endif /* (Sc2_SCB_IRQ__INTC_ASSIGNED) */

    Cy_SCB_SPI_Enable(Sc2_HW);
}

#if defined(__cplusplus)
}
#endif


/* [] END OF FILE */
