/*******************************************************************************
* File Name: contatore.c
* Version 1.0
*
* Description:
*  This file provides the source code to the API for the contatore
*  component
*
********************************************************************************
* Copyright 2016-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "contatore.h"

/** Indicates whether or not the contatore has been initialized. 
*  The variable is initialized to 0 and set to 1 the first time 
*  contatore_Start() is called. This allows the Component to 
*  restart without reinitialization after the first call to 
*  the contatore_Start() routine.
*/
uint8_t contatore_initVar = 0U;

/** The instance-specific configuration structure. This should be used in the 
*  associated contatore_Init() function.
*/ 
cy_stc_tcpwm_counter_config_t const contatore_config =
{
        .period = 32768UL,
        .clockPrescaler = 0UL,
        .runMode = 0UL,
        .countDirection = 0UL,
        .compareOrCapture = 2UL,
        .compare0 = 16384UL,
        .compare1 = 16384UL,
        .enableCompareSwap = false,
        .interruptSources = 0UL,
        .captureInputMode = 3UL,
        .captureInput = CY_TCPWM_INPUT_CREATOR,
        .reloadInputMode = 3UL,
        .reloadInput = CY_TCPWM_INPUT_CREATOR,
        .startInputMode = 3UL,
        .startInput = CY_TCPWM_INPUT_CREATOR,
        .stopInputMode = 3UL,
        .stopInput = CY_TCPWM_INPUT_CREATOR,
        .countInputMode = 0UL,
        .countInput = CY_TCPWM_INPUT_CREATOR,
};


/*******************************************************************************
* Function Name: contatore_Start
****************************************************************************//**
*
*  Calls the contatore_Init() when called the first time and enables 
*  the contatore. For subsequent calls the configuration is left 
*  unchanged and the component is just enabled.
*
* \globalvars
*  \ref contatore_initVar
*
*******************************************************************************/
void contatore_Start(void)
{
    if (0U == contatore_initVar)
    {
        (void)Cy_TCPWM_Counter_Init(contatore_HW, contatore_CNT_NUM, &contatore_config); 

        contatore_initVar = 1U;
    }

    Cy_TCPWM_Enable_Multiple(contatore_HW, contatore_CNT_MASK);
    
    #if (contatore_INPUT_DISABLED == 7UL)
        Cy_TCPWM_TriggerStart(contatore_HW, contatore_CNT_MASK);
    #endif /* (contatore_INPUT_DISABLED == 7UL) */    
}


/* [] END OF FILE */
