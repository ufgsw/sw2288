/***************************************************************************//**
* \file Seriale2.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(Seriale2_CY_SCB_UART_PDL_H)
#define Seriale2_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define Seriale2_DIRECTION  (3U)
#define Seriale2_ENABLE_RTS (0U)
#define Seriale2_ENABLE_CTS (0U)

/* UART direction enum */
#define Seriale2_RX    (0x1U)
#define Seriale2_TX    (0x2U)

#define Seriale2_ENABLE_RX  (0UL != (Seriale2_DIRECTION & Seriale2_RX))
#define Seriale2_ENABLE_TX  (0UL != (Seriale2_DIRECTION & Seriale2_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void Seriale2_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale2_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void Seriale2_DeInit(void);
__STATIC_INLINE void Seriale2_Enable(void);
__STATIC_INLINE void Seriale2_Disable(void);

/* Register callback. */
__STATIC_INLINE void Seriale2_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (Seriale2_ENABLE_CTS)
__STATIC_INLINE void Seriale2_EnableCts(void);
__STATIC_INLINE void Seriale2_DisableCts(void);
#endif /* (Seriale2_ENABLE_CTS) */

#if (Seriale2_ENABLE_RTS)
__STATIC_INLINE void     Seriale2_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t Seriale2_GetRtsFifoLevel(void);
#endif /* (Seriale2_ENABLE_RTS) */

__STATIC_INLINE void Seriale2_EnableSkipStart(void);
__STATIC_INLINE void Seriale2_DisableSkipStart(void);

#if (Seriale2_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t Seriale2_Get(void);
__STATIC_INLINE uint32_t Seriale2_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale2_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t Seriale2_GetRxFifoStatus(void);
__STATIC_INLINE void     Seriale2_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Seriale2_GetNumInRxFifo(void);
__STATIC_INLINE void     Seriale2_ClearRxFifo(void);
#endif /* (Seriale2_ENABLE_RX) */

#if (Seriale2_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t Seriale2_Put(uint32_t data);
__STATIC_INLINE uint32_t Seriale2_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale2_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale2_PutString(char_t const string[]);
__STATIC_INLINE void     Seriale2_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t Seriale2_GetTxFifoStatus(void);
__STATIC_INLINE void     Seriale2_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Seriale2_GetNumInTxFifo(void);
__STATIC_INLINE bool     Seriale2_IsTxComplete(void);
__STATIC_INLINE void     Seriale2_ClearTxFifo(void);
#endif /* (Seriale2_ENABLE_TX) */

#if (Seriale2_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     Seriale2_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale2_StopRingBuffer(void);
__STATIC_INLINE void     Seriale2_ClearRingBuffer(void);
__STATIC_INLINE uint32_t Seriale2_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale2_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale2_AbortReceive(void);
__STATIC_INLINE uint32_t Seriale2_GetReceiveStatus(void);
__STATIC_INLINE uint32_t Seriale2_GetNumReceived(void);
#endif /* (Seriale2_ENABLE_RX) */

#if (Seriale2_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale2_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale2_AbortTransmit(void);
__STATIC_INLINE uint32_t Seriale2_GetTransmitStatus(void);
__STATIC_INLINE uint32_t Seriale2_GetNumLeftToTransmit(void);
#endif /* (Seriale2_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void Seriale2_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t Seriale2_initVar;
extern cy_stc_scb_uart_config_t const Seriale2_config;
extern cy_stc_scb_uart_context_t Seriale2_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define Seriale2_HW     ((CySCB_Type *) Seriale2_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: Seriale2_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale2_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(Seriale2_HW, config, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_DeInit(void)
{
    Cy_SCB_UART_DeInit(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_Enable(void)
{
    Cy_SCB_UART_Enable(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_Disable(void)
{
    Cy_SCB_UART_Disable(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(Seriale2_HW, callback, &Seriale2_context);
}


#if (Seriale2_ENABLE_CTS)
/*******************************************************************************
* Function Name: Seriale2_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(Seriale2_HW);
}
#endif /* (Seriale2_ENABLE_CTS) */


#if (Seriale2_ENABLE_RTS)
/*******************************************************************************
* Function Name: Seriale2_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(Seriale2_HW, level);
}


/*******************************************************************************
* Function Name: Seriale2_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(Seriale2_HW);
}
#endif /* (Seriale2_ENABLE_RTS) */


/*******************************************************************************
* Function Name: Seriale2_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(Seriale2_HW);
}


#if (Seriale2_ENABLE_RX)
/*******************************************************************************
* Function Name: Seriale2_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_Get(void)
{
    return Cy_SCB_UART_Get(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(Seriale2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale2_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(Seriale2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale2_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(Seriale2_HW, clearMask);
}


/*******************************************************************************
* Function Name: Seriale2_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(Seriale2_HW);
}
#endif /* (Seriale2_ENABLE_RX) */


#if (Seriale2_ENABLE_TX)
/*******************************************************************************
* Function Name: Seriale2_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(Seriale2_HW,data);
}


/*******************************************************************************
* Function Name: Seriale2_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(Seriale2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale2_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(Seriale2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale2_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(Seriale2_HW, string);
}


/*******************************************************************************
* Function Name: Seriale2_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(Seriale2_HW, breakWidth);
}


/*******************************************************************************
* Function Name: Seriale2_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(Seriale2_HW, clearMask);
}


/*******************************************************************************
* Function Name: Seriale2_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool Seriale2_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(Seriale2_HW);
}


/*******************************************************************************
* Function Name: Seriale2_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(Seriale2_HW);
}
#endif /* (Seriale2_ENABLE_TX) */


#if (Seriale2_ENABLE_RX)
/*******************************************************************************
* Function Name: Seriale2_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(Seriale2_HW, buffer, size, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale2_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(Seriale2_HW, buffer, size, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(Seriale2_HW, &Seriale2_context);
}
#endif /* (Seriale2_ENABLE_RX) */


#if (Seriale2_ENABLE_TX)
/*******************************************************************************
* Function Name: Seriale2_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale2_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(Seriale2_HW, buffer, size, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(Seriale2_HW, &Seriale2_context);
}


/*******************************************************************************
* Function Name: Seriale2_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale2_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(Seriale2_HW, &Seriale2_context);
}
#endif /* (Seriale2_ENABLE_TX) */


/*******************************************************************************
* Function Name: Seriale2_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale2_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(Seriale2_HW, &Seriale2_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* Seriale2_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
