/***************************************************************************//**
* \file Seriale1.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(Seriale1_CY_SCB_UART_PDL_H)
#define Seriale1_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define Seriale1_DIRECTION  (3U)
#define Seriale1_ENABLE_RTS (0U)
#define Seriale1_ENABLE_CTS (0U)

/* UART direction enum */
#define Seriale1_RX    (0x1U)
#define Seriale1_TX    (0x2U)

#define Seriale1_ENABLE_RX  (0UL != (Seriale1_DIRECTION & Seriale1_RX))
#define Seriale1_ENABLE_TX  (0UL != (Seriale1_DIRECTION & Seriale1_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void Seriale1_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale1_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void Seriale1_DeInit(void);
__STATIC_INLINE void Seriale1_Enable(void);
__STATIC_INLINE void Seriale1_Disable(void);

/* Register callback. */
__STATIC_INLINE void Seriale1_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (Seriale1_ENABLE_CTS)
__STATIC_INLINE void Seriale1_EnableCts(void);
__STATIC_INLINE void Seriale1_DisableCts(void);
#endif /* (Seriale1_ENABLE_CTS) */

#if (Seriale1_ENABLE_RTS)
__STATIC_INLINE void     Seriale1_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t Seriale1_GetRtsFifoLevel(void);
#endif /* (Seriale1_ENABLE_RTS) */

__STATIC_INLINE void Seriale1_EnableSkipStart(void);
__STATIC_INLINE void Seriale1_DisableSkipStart(void);

#if (Seriale1_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t Seriale1_Get(void);
__STATIC_INLINE uint32_t Seriale1_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale1_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t Seriale1_GetRxFifoStatus(void);
__STATIC_INLINE void     Seriale1_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Seriale1_GetNumInRxFifo(void);
__STATIC_INLINE void     Seriale1_ClearRxFifo(void);
#endif /* (Seriale1_ENABLE_RX) */

#if (Seriale1_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t Seriale1_Put(uint32_t data);
__STATIC_INLINE uint32_t Seriale1_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale1_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale1_PutString(char_t const string[]);
__STATIC_INLINE void     Seriale1_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t Seriale1_GetTxFifoStatus(void);
__STATIC_INLINE void     Seriale1_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Seriale1_GetNumInTxFifo(void);
__STATIC_INLINE bool     Seriale1_IsTxComplete(void);
__STATIC_INLINE void     Seriale1_ClearTxFifo(void);
#endif /* (Seriale1_ENABLE_TX) */

#if (Seriale1_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     Seriale1_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale1_StopRingBuffer(void);
__STATIC_INLINE void     Seriale1_ClearRingBuffer(void);
__STATIC_INLINE uint32_t Seriale1_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale1_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale1_AbortReceive(void);
__STATIC_INLINE uint32_t Seriale1_GetReceiveStatus(void);
__STATIC_INLINE uint32_t Seriale1_GetNumReceived(void);
#endif /* (Seriale1_ENABLE_RX) */

#if (Seriale1_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale1_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale1_AbortTransmit(void);
__STATIC_INLINE uint32_t Seriale1_GetTransmitStatus(void);
__STATIC_INLINE uint32_t Seriale1_GetNumLeftToTransmit(void);
#endif /* (Seriale1_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void Seriale1_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t Seriale1_initVar;
extern cy_stc_scb_uart_config_t const Seriale1_config;
extern cy_stc_scb_uart_context_t Seriale1_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define Seriale1_HW     ((CySCB_Type *) Seriale1_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: Seriale1_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale1_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(Seriale1_HW, config, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_DeInit(void)
{
    Cy_SCB_UART_DeInit(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_Enable(void)
{
    Cy_SCB_UART_Enable(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_Disable(void)
{
    Cy_SCB_UART_Disable(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(Seriale1_HW, callback, &Seriale1_context);
}


#if (Seriale1_ENABLE_CTS)
/*******************************************************************************
* Function Name: Seriale1_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(Seriale1_HW);
}
#endif /* (Seriale1_ENABLE_CTS) */


#if (Seriale1_ENABLE_RTS)
/*******************************************************************************
* Function Name: Seriale1_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(Seriale1_HW, level);
}


/*******************************************************************************
* Function Name: Seriale1_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(Seriale1_HW);
}
#endif /* (Seriale1_ENABLE_RTS) */


/*******************************************************************************
* Function Name: Seriale1_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(Seriale1_HW);
}


#if (Seriale1_ENABLE_RX)
/*******************************************************************************
* Function Name: Seriale1_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_Get(void)
{
    return Cy_SCB_UART_Get(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(Seriale1_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale1_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(Seriale1_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale1_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(Seriale1_HW, clearMask);
}


/*******************************************************************************
* Function Name: Seriale1_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(Seriale1_HW);
}
#endif /* (Seriale1_ENABLE_RX) */


#if (Seriale1_ENABLE_TX)
/*******************************************************************************
* Function Name: Seriale1_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(Seriale1_HW,data);
}


/*******************************************************************************
* Function Name: Seriale1_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(Seriale1_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale1_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(Seriale1_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale1_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(Seriale1_HW, string);
}


/*******************************************************************************
* Function Name: Seriale1_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(Seriale1_HW, breakWidth);
}


/*******************************************************************************
* Function Name: Seriale1_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(Seriale1_HW, clearMask);
}


/*******************************************************************************
* Function Name: Seriale1_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool Seriale1_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(Seriale1_HW);
}


/*******************************************************************************
* Function Name: Seriale1_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(Seriale1_HW);
}
#endif /* (Seriale1_ENABLE_TX) */


#if (Seriale1_ENABLE_RX)
/*******************************************************************************
* Function Name: Seriale1_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(Seriale1_HW, buffer, size, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale1_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(Seriale1_HW, buffer, size, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(Seriale1_HW, &Seriale1_context);
}
#endif /* (Seriale1_ENABLE_RX) */


#if (Seriale1_ENABLE_TX)
/*******************************************************************************
* Function Name: Seriale1_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale1_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(Seriale1_HW, buffer, size, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(Seriale1_HW, &Seriale1_context);
}


/*******************************************************************************
* Function Name: Seriale1_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale1_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(Seriale1_HW, &Seriale1_context);
}
#endif /* (Seriale1_ENABLE_TX) */


/*******************************************************************************
* Function Name: Seriale1_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale1_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(Seriale1_HW, &Seriale1_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* Seriale1_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
