/***************************************************************************//**
* \file Sc1.c
* \version 2.0
*
*  This file provides constants and parameter values for the I2C component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(Sc1_CY_SCB_I2C_PDL_H)
#define Sc1_CY_SCB_I2C_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_i2c.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define Sc1_MODE               (0x2U)
#define Sc1_MODE_SLAVE_MASK    (0x1U)
#define Sc1_MODE_MASTER_MASK   (0x2U)

#define Sc1_ENABLE_SLAVE       (0UL != (Sc1_MODE & Sc1_MODE_SLAVE_MASK))
#define Sc1_ENABLE_MASTER      (0UL != (Sc1_MODE & Sc1_MODE_MASTER_MASK))
#define Sc1_MANUAL_SCL_CONTROL (0U)


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component only APIs. */
void Sc1_Start(void);

/* Basic functions. */
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_Init(cy_stc_scb_i2c_config_t const *config);
__STATIC_INLINE void Sc1_DeInit (void);
__STATIC_INLINE void Sc1_Enable (void);
__STATIC_INLINE void Sc1_Disable(void);

/* Data rate configuration functions. */
__STATIC_INLINE uint32_t Sc1_SetDataRate(uint32_t dataRateHz, uint32_t scbClockHz);
__STATIC_INLINE uint32_t Sc1_GetDataRate(uint32_t scbClockHz);

/* Register callbacks. */
__STATIC_INLINE void Sc1_RegisterEventCallback(cy_cb_scb_i2c_handle_events_t callback);
#if (Sc1_ENABLE_SLAVE)
__STATIC_INLINE void Sc1_RegisterAddrCallback (cy_cb_scb_i2c_handle_addr_t callback);
#endif /* (Sc1_ENABLE_SLAVE) */

/* Configuration functions. */
#if (Sc1_ENABLE_SLAVE)
__STATIC_INLINE void     Sc1_SlaveSetAddress(uint8_t addr);
__STATIC_INLINE uint32_t Sc1_SlaveGetAddress(void);
__STATIC_INLINE void     Sc1_SlaveSetAddressMask(uint8_t addrMask);
__STATIC_INLINE uint32_t Sc1_SlaveGetAddressMask(void);
#endif /* (Sc1_ENABLE_SLAVE) */

#if (Sc1_ENABLE_MASTER)
__STATIC_INLINE void Sc1_MasterSetLowPhaseDutyCycle (uint32_t clockCycles);
__STATIC_INLINE void Sc1_MasterSetHighPhaseDutyCycle(uint32_t clockCycles);
#endif /* (Sc1_ENABLE_MASTER) */

/* Bus status. */
__STATIC_INLINE bool     Sc1_IsBusBusy(void);

/* Slave functions. */
#if (Sc1_ENABLE_SLAVE)
__STATIC_INLINE uint32_t Sc1_SlaveGetStatus(void);

__STATIC_INLINE void     Sc1_SlaveConfigReadBuf(uint8_t *buffer, uint32_t size);
__STATIC_INLINE void     Sc1_SlaveAbortRead(void);
__STATIC_INLINE uint32_t Sc1_SlaveGetReadTransferCount(void);
__STATIC_INLINE uint32_t Sc1_SlaveClearReadStatus(void);

__STATIC_INLINE void     Sc1_SlaveConfigWriteBuf(uint8_t *buffer, uint32_t size);
__STATIC_INLINE void     Sc1_SlaveAbortWrite(void);
__STATIC_INLINE uint32_t Sc1_SlaveGetWriteTransferCount(void);
__STATIC_INLINE uint32_t Sc1_SlaveClearWriteStatus(void);
#endif /* (Sc1_ENABLE_SLAVE) */

/* Master interrupt processing functions. */
#if (Sc1_ENABLE_MASTER)
__STATIC_INLINE uint32_t Sc1_MasterGetStatus(void);

__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterRead(cy_stc_scb_i2c_master_xfer_config_t *xferConfig);
__STATIC_INLINE void Sc1_MasterAbortRead(void);
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterWrite(cy_stc_scb_i2c_master_xfer_config_t *xferConfig);
__STATIC_INLINE void Sc1_MasterAbortWrite(void);
__STATIC_INLINE uint32_t Sc1_MasterGetTransferCount(void);

/* Master manual processing functions. */
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterSendStart(uint32_t address, cy_en_scb_i2c_direction_t bitRnW, uint32_t timeoutMs);
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterSendReStart(uint32_t address, cy_en_scb_i2c_direction_t bitRnW, uint32_t timeoutMs);
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterSendStop(uint32_t timeoutMs);
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterReadByte(cy_en_scb_i2c_command_t ackNack, uint8_t *byte, uint32_t timeoutMs);
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterWriteByte(uint8_t byte, uint32_t timeoutMs);
#endif /* (Sc1_ENABLE_MASTER) */

/* Interrupt handler. */
__STATIC_INLINE void Sc1_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t Sc1_initVar;
extern cy_stc_scb_i2c_config_t const Sc1_config;
extern cy_stc_scb_i2c_context_t Sc1_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the SCB instance */
#define Sc1_HW     ((CySCB_Type *) Sc1_SCB__HW)

/** The desired data rate in Hz */
#define Sc1_DATA_RATE_HZ      (100000U)

/** The frequency of the clock used by the Component in Hz */
#define Sc1_CLK_FREQ_HZ       (1600000U)

/** The number of Component clocks used by the master to generate the SCL
* low phase. This number is calculated by GUI based on the selected data rate.
*/
#define Sc1_LOW_PHASE_DUTY_CYCLE   (8U)

/** The number of Component clocks used by the master to generate the SCL
* high phase. This number is calculated by GUI based on the selected data rate.
*/
#define Sc1_HIGH_PHASE_DUTY_CYCLE  (8U)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: Sc1_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_Init(cy_stc_scb_i2c_config_t const *config)
{
    return Cy_SCB_I2C_Init(Sc1_HW, config, &Sc1_context);
}


/*******************************************************************************
*  Function Name: Sc1_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_DeInit(void)
{
    Cy_SCB_I2C_DeInit(Sc1_HW);
}


/*******************************************************************************
* Function Name: Sc1_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_Enable(void)
{
    Cy_SCB_I2C_Enable(Sc1_HW);
}


/*******************************************************************************
* Function Name: Sc1_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_Disable(void)
{
    Cy_SCB_I2C_Disable(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SetDataRate
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SetDataRate() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SetDataRate(uint32_t dataRateHz, uint32_t scbClockHz)
{
    return Cy_SCB_I2C_SetDataRate(Sc1_HW, dataRateHz, scbClockHz);
}


/*******************************************************************************
* Function Name: Sc1_GetDataRate
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_GetDataRate() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_GetDataRate(uint32_t scbClockHz)
{
    return Cy_SCB_I2C_GetDataRate(Sc1_HW, scbClockHz);
}


/*******************************************************************************
* Function Name: Sc1_RegisterEventCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_RegisterEventCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_RegisterEventCallback(cy_cb_scb_i2c_handle_events_t callback)
{
    Cy_SCB_I2C_RegisterEventCallback(Sc1_HW, callback, &Sc1_context);
}


#if (Sc1_ENABLE_SLAVE)
/*******************************************************************************
* Function Name: Sc1_RegisterAddrCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_RegisterAddrCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_RegisterAddrCallback(cy_cb_scb_i2c_handle_addr_t callback)
{
    Cy_SCB_I2C_RegisterAddrCallback(Sc1_HW, callback, &Sc1_context);
}
#endif /* (Sc1_ENABLE_SLAVE) */


/*******************************************************************************
* Function Name: Sc1_IsBusBusy
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_IsBusBusy() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool Sc1_IsBusBusy(void)
{
    return Cy_SCB_I2C_IsBusBusy(Sc1_HW);
}


#if (Sc1_ENABLE_SLAVE)
/*******************************************************************************
* Function Name: Sc1_SlaveSetAddress
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveGetAddress() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_SlaveSetAddress(uint8_t addr)
{
    Cy_SCB_I2C_SlaveSetAddress(Sc1_HW, addr);
}


/*******************************************************************************
* Function Name: Sc1_SlaveGetAddress
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveGetAddress() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveGetAddress(void)
{
    return Cy_SCB_I2C_SlaveGetAddress(Sc1_HW);
}


/*******************************************************************************
* Function Name: Sc1_SlaveSetAddressMask
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveSetAddressMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_SlaveSetAddressMask(uint8_t addrMask)
{
    Cy_SCB_I2C_SlaveSetAddressMask(Sc1_HW, addrMask);
}


/*******************************************************************************
* Function Name: Sc1_SlaveGetAddressMask
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveGetAddressMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveGetAddressMask(void)
{
    return Cy_SCB_I2C_SlaveGetAddressMask(Sc1_HW);
}
#endif /* (Sc1_ENABLE_SLAVE) */

#if (Sc1_ENABLE_MASTER)
/*******************************************************************************
* Function Name: Sc1_MasterSetLowPhaseDutyCycle
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterSetLowPhaseDutyCycle() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_MasterSetLowPhaseDutyCycle(uint32_t clockCycles)
{
    Cy_SCB_I2C_MasterSetLowPhaseDutyCycle(Sc1_HW, clockCycles);
}


/*******************************************************************************
* Function Name: Sc1_MasterSetHighPhaseDutyCycle
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterSetHighPhaseDutyCycle() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_MasterSetHighPhaseDutyCycle(uint32_t clockCycles)
{
    Cy_SCB_I2C_MasterSetHighPhaseDutyCycle(Sc1_HW, clockCycles);
}
#endif /* (Sc1_ENABLE_MASTER) */


#if (Sc1_ENABLE_SLAVE)
/*******************************************************************************
* Function Name: Sc1_SlaveGetStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveGetStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveGetStatus(void)
{
    return Cy_SCB_I2C_SlaveGetStatus(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveConfigReadBuf
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveConfigReadBuf() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_SlaveConfigReadBuf(uint8_t *buffer, uint32_t size)
{
    Cy_SCB_I2C_SlaveConfigReadBuf(Sc1_HW, buffer, size, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveAbortRead
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveAbortRead() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_SlaveAbortRead(void)
{
    Cy_SCB_I2C_SlaveAbortRead(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveGetReadTransferCount
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveGetReadTransferCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveGetReadTransferCount(void)
{
    return Cy_SCB_I2C_SlaveGetReadTransferCount(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveClearReadStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveClearReadStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveClearReadStatus(void)
{
    return Cy_SCB_I2C_SlaveClearReadStatus(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveConfigWriteBuf
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveConfigWriteBuf() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_SlaveConfigWriteBuf(uint8_t *buffer, uint32_t size)
{
    Cy_SCB_I2C_SlaveConfigWriteBuf(Sc1_HW, buffer, size, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveAbortWrite
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveAbortWrite() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_SlaveAbortWrite(void)
{
    Cy_SCB_I2C_SlaveAbortWrite(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveGetWriteTransferCount
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveGetWriteTransferCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveGetWriteTransferCount(void)
{
    return Cy_SCB_I2C_SlaveGetWriteTransferCount(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_SlaveClearWriteStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_SlaveClearWriteStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_SlaveClearWriteStatus(void)
{
    return Cy_SCB_I2C_SlaveClearWriteStatus(Sc1_HW, &Sc1_context);
}
#endif /* (Sc1_ENABLE_SLAVE) */


#if (Sc1_ENABLE_MASTER)
/*******************************************************************************
* Function Name: Sc1_MasterGetStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterGetStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_MasterGetStatus(void)
{
    return Cy_SCB_I2C_MasterGetStatus(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterRead
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterRead() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterRead(cy_stc_scb_i2c_master_xfer_config_t *xferConfig)
{
    return Cy_SCB_I2C_MasterRead(Sc1_HW, xferConfig, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterAbortRead
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterAbortRead() PDL driver function.
*
******************************************************************************/
__STATIC_INLINE void Sc1_MasterAbortRead(void)
{
    Cy_SCB_I2C_MasterAbortRead(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterWrite
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterWrite() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterWrite(cy_stc_scb_i2c_master_xfer_config_t *xferConfig)
{
    return Cy_SCB_I2C_MasterWrite(Sc1_HW, xferConfig, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterAbortWrite
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterAbortWrite() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_MasterAbortWrite(void)
{
    Cy_SCB_I2C_MasterAbortWrite(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterGetTransferCount
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterGetTransferCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc1_MasterGetTransferCount(void)
{
    return Cy_SCB_I2C_MasterGetTransferCount(Sc1_HW, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterSendStart
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterSendStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterSendStart(uint32_t address, cy_en_scb_i2c_direction_t bitRnW, uint32_t timeoutMs)
{
    return Cy_SCB_I2C_MasterSendStart(Sc1_HW, address, bitRnW, timeoutMs, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterSendReStart
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterSendReStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterSendReStart(uint32_t address, cy_en_scb_i2c_direction_t bitRnW, uint32_t timeoutMs)
{
    return Cy_SCB_I2C_MasterSendReStart(Sc1_HW, address, bitRnW, timeoutMs, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterSendStop
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterSendStop() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterSendStop(uint32_t timeoutMs)
{
    return Cy_SCB_I2C_MasterSendStop(Sc1_HW, timeoutMs, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterReadByte
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterReadByte() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterReadByte(cy_en_scb_i2c_command_t ackNack, uint8_t *byte, uint32_t timeoutMs)
{
    return Cy_SCB_I2C_MasterReadByte(Sc1_HW, ackNack, byte, timeoutMs, &Sc1_context);
}


/*******************************************************************************
* Function Name: Sc1_MasterWriteByte
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_MasterWriteByte() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_i2c_status_t Sc1_MasterWriteByte(uint8_t byte, uint32_t timeoutMs)
{
    return Cy_SCB_I2C_MasterWriteByte(Sc1_HW, byte, timeoutMs, &Sc1_context);
}
#endif /* (Sc1_ENABLE_MASTER) */


/*******************************************************************************
* Function Name: Sc1_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_I2C_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc1_Interrupt(void)
{
    Cy_SCB_I2C_Interrupt(Sc1_HW, &Sc1_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* Sc1_CY_SCB_I2C_PDL_H */


/* [] END OF FILE */
