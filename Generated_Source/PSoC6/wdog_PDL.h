/*******************************************************************************
* File Name: wdog.h
* Version 1.10
*
* Description:
*  This file provides constants and parameter values for the wdog
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(wdog_CY_MCWDT_PDL_H)
#define wdog_CY_MCWDT_PDL_H

#if defined(__cplusplus)
extern "C" {
#endif

#include "cyfitter.h"
#include "mcwdt/cy_mcwdt.h"

/*******************************************************************************
*   Variables
*******************************************************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t  wdog_initVar;
extern const cy_stc_mcwdt_config_t wdog_config;
/** @} group_globals */

/***************************************
*   Conditional Compilation Parameters
****************************************/
#define wdog_C0_CLEAR_ON_MATCH  (1U)
#define wdog_C1_CLEAR_ON_MATCH  (1U)
#define wdog_CASCADE_C0C1       (1U)
#define wdog_CASCADE_C1C2       (0U)
#define wdog_C0_MATCH           (32768U)
#define wdog_C0_MODE            (0U)
#define wdog_C1_MATCH           (5U)
#define wdog_C1_MODE            (2U)
#define wdog_C2_PERIOD          (16U)
#define wdog_C2_MODE            (0U)

#if (0u == 1U)
    #define wdog_CTR0_EN_MASK   0UL
#else
    #define wdog_CTR0_EN_MASK   CY_MCWDT_CTR0
#endif
#if (0u == 1U)
    #define wdog_CTR1_EN_MASK   0UL
#else
    #define wdog_CTR1_EN_MASK   CY_MCWDT_CTR1
#endif
#if (0u == 0U)
    #define wdog_CTR2_EN_MASK   0UL
#else
    #define wdog_CTR2_EN_MASK   CY_MCWDT_CTR2
#endif

#define wdog_ENABLED_CTRS_MASK  (wdog_CTR0_EN_MASK |\
                                             wdog_CTR1_EN_MASK |\
                                             wdog_CTR2_EN_MASK)
											 
#if (1U == wdog_C0_MODE) || (3U == wdog_C0_MODE)
    #define wdog_CTR0_INT_MASK   CY_MCWDT_CTR0
#else
    #define wdog_CTR0_INT_MASK   0UL
#endif
#if (1U == wdog_C1_MODE) || (3U == wdog_C1_MODE)
    #define wdog_CTR1_INT_MASK   CY_MCWDT_CTR1
#else
    #define wdog_CTR1_INT_MASK   0UL
#endif
#if (1U == wdog_C2_MODE)
    #define wdog_CTR2_INT_MASK   CY_MCWDT_CTR2
#else
    #define wdog_CTR2_INT_MASK   0UL
#endif 

#define wdog_CTRS_INT_MASK      (wdog_CTR0_INT_MASK |\
                                             wdog_CTR1_INT_MASK |\
                                             wdog_CTR2_INT_MASK)										 

/***************************************
*        Registers Constants
***************************************/

/* This is a ptr to the base address of the MCWDT instance. */
#define wdog_HW                 (wdog_MCWDT__HW)

#if (0u == wdog_MCWDT__IDX)
    #define wdog_RESET_REASON   CY_SYSLIB_RESET_SWWDT0
#else
    #define wdog_RESET_REASON   CY_SYSLIB_RESET_SWWDT1
#endif 

#define wdog_TWO_LF_CLK_CYCLES_DELAY (62u)


/*******************************************************************************
*        Function Prototypes
*******************************************************************************/
/**
* \addtogroup group_general
* @{
*/
                void     wdog_Start(void);
                void     wdog_Stop(void);
__STATIC_INLINE cy_en_mcwdt_status_t wdog_Init(const cy_stc_mcwdt_config_t *config);
__STATIC_INLINE void     wdog_DeInit(void);
__STATIC_INLINE void     wdog_Enable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE void     wdog_Disable(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t wdog_GetEnabledStatus(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdog_Lock(void);
__STATIC_INLINE void     wdog_Unlock(void);
__STATIC_INLINE uint32_t wdog_GetLockedStatus(void);
__STATIC_INLINE void     wdog_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode);
__STATIC_INLINE cy_en_mcwdtmode_t wdog_GetMode(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdog_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable);
__STATIC_INLINE uint32_t wdog_GetClearOnMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdog_SetCascade(cy_en_mcwdtcascade_t cascade);
__STATIC_INLINE cy_en_mcwdtcascade_t wdog_GetCascade(void);
__STATIC_INLINE void     wdog_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs);
__STATIC_INLINE uint32_t wdog_GetMatch(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdog_SetToggleBit(uint32_t bit);
__STATIC_INLINE uint32_t wdog_GetToggleBit(void);
__STATIC_INLINE uint32_t wdog_GetCount(cy_en_mcwdtctr_t counter);
__STATIC_INLINE void     wdog_ResetCounters(uint32_t counters, uint16_t waitUs);
__STATIC_INLINE uint32_t wdog_GetInterruptStatus(void);
__STATIC_INLINE void     wdog_ClearInterrupt(uint32_t counters);
__STATIC_INLINE void     wdog_SetInterrupt(uint32_t counters);
__STATIC_INLINE uint32_t wdog_GetInterruptMask(void);
__STATIC_INLINE void     wdog_SetInterruptMask(uint32_t counters);
__STATIC_INLINE uint32_t wdog_GetInterruptStatusMasked(void);
__STATIC_INLINE uint32_t wdog_GetCountCascaded(void);
/** @} general */


/*******************************************************************************
* Function Name: wdog_Init
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdt_status_t wdog_Init(const cy_stc_mcwdt_config_t *config)
{
    return (Cy_MCWDT_Init(wdog_HW, config));
}


/*******************************************************************************
* Function Name: wdog_DeInit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_DeInit(void)
{
    Cy_MCWDT_DeInit(wdog_HW);
}


/*******************************************************************************
* Function Name: wdog_Enable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_Enable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Enable(wdog_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: wdog_Disable
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_Disable(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_Disable(wdog_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: wdog_GetEnabledStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetEnabledStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetEnabledStatus(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetEnabledStatus(wdog_HW, counter));
}


/*******************************************************************************
* Function Name: wdog_Lock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Lock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_Lock(void)
{
    Cy_MCWDT_Lock(wdog_HW);
}


/*******************************************************************************
* Function Name: wdog_Unlock
****************************************************************************//**
*
* Invokes the Cy_MCWDT_Unlock() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_Unlock(void)
{
   Cy_MCWDT_Unlock(wdog_HW);
}


/*******************************************************************************
* Function Name: wdog_GetLockStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetLockedStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetLockedStatus(void)
{
    return (Cy_MCWDT_GetLockedStatus(wdog_HW));
}


/*******************************************************************************
* Function Name: wdog_SetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetMode(cy_en_mcwdtctr_t counter, cy_en_mcwdtmode_t mode)
{
    Cy_MCWDT_SetMode(wdog_HW, counter, mode);
}


/*******************************************************************************
* Function Name: wdog_GetMode
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMode() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtmode_t wdog_GetMode(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMode(wdog_HW, counter));
}


/*******************************************************************************
* Function Name: wdog_SetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetClearOnMatch(cy_en_mcwdtctr_t counter, uint32_t enable)
{
    Cy_MCWDT_SetClearOnMatch(wdog_HW, counter, enable);
}


/*******************************************************************************
* Function Name: wdog_GetClearOnMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetClearOnMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetClearOnMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetClearOnMatch(wdog_HW, counter));
}


/*******************************************************************************
* Function Name: wdog_SetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetCascade(cy_en_mcwdtcascade_t cascade)
{
    Cy_MCWDT_SetCascade(wdog_HW, cascade);
}


/*******************************************************************************
* Function Name: wdog_GetCascade
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCascade() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_mcwdtcascade_t wdog_GetCascade(void)
{
    return (Cy_MCWDT_GetCascade(wdog_HW));
}


/*******************************************************************************
* Function Name: wdog_SetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetMatch(cy_en_mcwdtctr_t counter, uint32_t match, uint16_t waitUs)
{
    Cy_MCWDT_SetMatch(wdog_HW, counter, match, waitUs);
}


/*******************************************************************************
* Function Name: wdog_GetMatch
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetMatch() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetMatch(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetMatch(wdog_HW, counter));
}


/*******************************************************************************
* Function Name: wdog_SetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetToggleBit(uint32_t bit)
{
    Cy_MCWDT_SetToggleBit(wdog_HW, bit);
}

/*******************************************************************************
* Function Name: wdog_GetToggleBit
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetToggleBit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetToggleBit(void)
{
    return (Cy_MCWDT_GetToggleBit(wdog_HW));
}


/*******************************************************************************
* Function Name: wdog_GetCount
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetCount() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetCount(cy_en_mcwdtctr_t counter)
{
    return (Cy_MCWDT_GetCount(wdog_HW, counter));
}


/*******************************************************************************
* Function Name: wdog_ResetCounters
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ResetCounters() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_ResetCounters(uint32_t counters, uint16_t waitUs)
{
    Cy_MCWDT_ResetCounters(wdog_HW, counters, waitUs);
}


/*******************************************************************************
* Function Name: wdog_GetInterruptStatus
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetInterruptStatus(void)
{
    return (Cy_MCWDT_GetInterruptStatus(wdog_HW));
}


/*******************************************************************************
* Function Name: wdog_ClearInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_ClearInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_ClearInterrupt(uint32_t counters)
{
    Cy_MCWDT_ClearInterrupt(wdog_HW, counters);
}


/*******************************************************************************
* Function Name: wdog_SetInterrupt
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetInterrupt(uint32_t counters)
{
    Cy_MCWDT_SetInterrupt(wdog_HW, counters);
}


/*******************************************************************************
* Function Name: wdog_GetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_GetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetInterruptMask(void)
{
    return (Cy_MCWDT_GetInterruptMask(wdog_HW));
}


/*******************************************************************************
* Function Name: wdog_SetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_MCWDT_SetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void wdog_SetInterruptMask(uint32_t counters)
{
    Cy_MCWDT_SetInterruptMask(wdog_HW, counters);
}


/*******************************************************************************
* Function Name: wdog_GetInterruptStatusMasked
****************************************************************************//**
* Invokes the Cy_MCWDT_GetInterruptStatusMasked() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetInterruptStatusMasked(void)
{
    return (Cy_MCWDT_GetInterruptStatusMasked(wdog_HW));
}


/*******************************************************************************
* Function Name: wdog_GetCountCascaded
****************************************************************************//**
* Invokes the Cy_MCWDT_GetCountCascaded() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t wdog_GetCountCascaded(void)
{
    return (Cy_MCWDT_GetCountCascaded(wdog_HW));
}

#if defined(__cplusplus)
}
#endif

#endif /* wdog_CY_MCWDT_PDL_H */


/* [] END OF FILE */
