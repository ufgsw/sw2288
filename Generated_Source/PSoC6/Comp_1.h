/*******************************************************************************
* File Name: Comp_1.h
* Version 1.0
*
* Description:
*  This file provides constants and parameter values for the Comp_1
*  component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_Comp_1_H)
#define CY_Comp_1_H

#include <cy_device_headers.h>
#include <ctb/cy_ctb.h>

/*******************************************************************************
                            Internal Constants
*******************************************************************************/
#define Comp_1_CTB_HW                         CTBM0
#define Comp_1_COMP_NUM                       ((Comp_1_cy_mxs40_opamp__OA_IDX == 0u) \
                                                        ? CY_CTB_OPAMP_0 : CY_CTB_OPAMP_1)

/*******************************************************************************
                                  Parameters
The parameters that are set in the customizer are redefined as constants here.
*******************************************************************************/
#define Comp_1_PARAM_Power                    (2UL)
#define Comp_1_PARAM_Interrupt                (1UL)
#define Comp_1_PARAM_Hysteresis               (1UL)
#define Comp_1_PARAM_DeepSleep                (0UL)

/*******************************************************************************
                    Variables with External Linkage
*******************************************************************************/

/** Tracks whether block is initialized (1) or not (0). */
extern uint8_t Comp_1_initVar;

/** Tracks the power level setting. Initialized to the power level setting in the customizer. */
extern cy_en_ctb_power_t Comp_1_compPower;

/** Configuration structure for initializing one comparator using the CTB PDL. */
extern const cy_stc_ctb_opamp_config_t Comp_1_compConfig;

/*******************************************************************************
                      Comparator Configuration Defines
Constants used in the configuration structure for initializing the comparator.
*******************************************************************************/
#if (CYDEV_SYSTEM_AREF_CURRENT == CYDEV_SYSTEM_AREF_CURRENT_LOW)
    /* If the low reference current is used, disable the charge pump. */
    #define Comp_1_COMP_CHARGEPUMP            CY_CTB_PUMP_DISABLE
#else
    /* Charge pump configuration depends on Deep Sleep selection in the customizer. */
    #define Comp_1_COMP_CHARGEPUMP            CY_CTB_PUMP_ENABLE
#endif
/*******************************************************************************
                        Function Prototypes
*******************************************************************************/
void Comp_1_Start(void);
__STATIC_INLINE void Comp_1_Init(void);
__STATIC_INLINE void Comp_1_Enable(void);
__STATIC_INLINE void Comp_1_Stop(void);
__STATIC_INLINE void Comp_1_SetPower(cy_en_ctb_power_t power);
__STATIC_INLINE void Comp_1_SetDeepSleepMode(cy_en_ctb_deep_sleep_t deepSleep);
__STATIC_INLINE void Comp_1_SetInterruptEdgeType(cy_en_ctb_comp_edge_t edge);
__STATIC_INLINE uint32_t Comp_1_GetStatus(void);

__STATIC_INLINE void Comp_1_EnableInterrupt(void);
__STATIC_INLINE void Comp_1_DisableInterrupt(void);
__STATIC_INLINE uint32_t Comp_1_GetInterruptStatus(void);
__STATIC_INLINE void Comp_1_ClearInterrupt(void);
__STATIC_INLINE void Comp_1_SetInterrupt(void);
__STATIC_INLINE uint32_t Comp_1_GetInterruptMask(void);
__STATIC_INLINE uint32_t Comp_1_GetInterruptStatusMasked(void);

/*******************************************************************************
* Function Name: Comp_1_Init
****************************************************************************//**
*
* Initialize the component according to the customizer settings.
* This function can also be used to restore the customizer settings
* if they are changed at run time.
*
* It is not necessary to call Comp_1_Init() because
* Comp_1_Start() calls this function. Comp_1_Start() is the
* preferred method to begin component operation.
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_Init(void)
{
    (void) Cy_CTB_OpampInit(Comp_1_CTB_HW, Comp_1_COMP_NUM, &Comp_1_compConfig);
}

/*******************************************************************************
* Function Name: Comp_1_Enable
****************************************************************************//**
*
* Turn on the CTB and set the comparator power level based on the last call to
* Comp_1_SetPower(). If Comp_1_SetPower() was never called,
* the power level set in the customizer is used.
*
* The other half of the CTB block is unaffected.
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_Enable(void)
{
    Cy_CTB_Enable(Comp_1_CTB_HW);
    Cy_CTB_SetPower(Comp_1_CTB_HW, Comp_1_COMP_NUM, Comp_1_compPower, Comp_1_COMP_CHARGEPUMP);
}

/*******************************************************************************
* Function Name: Comp_1_Stop
****************************************************************************//**
*
* Turn off the comparator.
*
* The other half of the CTB block is unaffected.
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_Stop(void)
{
    Cy_CTB_SetPower(Comp_1_CTB_HW, Comp_1_COMP_NUM, CY_CTB_POWER_OFF, Comp_1_COMP_CHARGEPUMP);
}

/*******************************************************************************
* Function Name: Comp_1_SetPower
****************************************************************************//**
*
* Set the power level of the comparator.
*
* \param power
* - CY_CTB_POWER_OFF: This input option does nothing. Comp_1_Stop()
*   is the preferred method to turn off the comparator.
* - CY_CTB_POWER_LOW: Low power/speed level
* - CY_CTB_POWER_MEDIUM: Medium power/speed level
* - CY_CTB_POWER_HIGH: High power/speed level
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_SetPower(cy_en_ctb_power_t power)
{
    if (CY_CTB_POWER_OFF != power)
    {
        Cy_CTB_SetPower(Comp_1_CTB_HW, Comp_1_COMP_NUM, power, Comp_1_COMP_CHARGEPUMP);

        /* Update the tracking variable for the comparator power level. */
        Comp_1_compPower = power;
    }
}

/*******************************************************************************
* Function Name: Comp_1_SetDeepSleepMode
****************************************************************************//**
*
* Set whether the entire CTB block continues to stay powered in Deep Sleep mode.
* This impacts both opamps in the CTB. If there are multiple instances of
* the Comparator component or other components that use the opamp resource
* (for example, Opamp or VDAC12 components), this will affect all instances.
*
* If Comp_1_Stop() is called before entering Deep Sleep mode,
* the comparator will remain off in Deep Sleep.
*
* Deep Sleep should not be enabled at run time if it was not enabled
* in the customizer. Analog routing and AREF block configuration are performed
* at build time. If Deep Sleep is not enabled in the customizer at build time
* the AREF block and the analog route may not be available during Deep Sleep.
*
* \param deepSleep
* - CY_CTB_DEEPSLEEP_DISABLE: Disable entire CTB is Deep Sleep mode
* - CY_CTB_DEEPSLEEP_ENABLE: Enable entire CTB in Deep Sleep mode
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_SetDeepSleepMode(cy_en_ctb_deep_sleep_t deepSleep)
{
    Cy_CTB_SetDeepSleepMode(Comp_1_CTB_HW, deepSleep);
}

/*******************************************************************************
* Function Name: Comp_1_SetInterruptEdgeType
****************************************************************************//**
*
* Configure the edge type that will trigger an interrupt.
*
* \param edge
* - CY_CTB_COMP_EDGE_DISABLE: No interrupts are generated
* - CY_CTB_COMP_EDGE_RISING: Rising edge generates an interrupt
* - CY_CTB_COMP_EDGE_FALLING: Falling edge generates an interrupt
* - CY_CTB_COMP_EDGE_BOTH: Both edges generate an interrupt
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_SetInterruptEdgeType(cy_en_ctb_comp_edge_t edge)
{
    Cy_CTB_CompSetInterruptEdgeType(Comp_1_CTB_HW, Comp_1_COMP_NUM, edge);
}

/*******************************************************************************
* Function Name: Comp_1_GetStatus
****************************************************************************//**
*
* Return the status of the comparator output.
* The comparator status is high when the positive input voltage is greater
* than the negative input voltage.
*
* \return Comparator status
* - 0: Comparator output low
* - 1: Comparator output high
*
*******************************************************************************/
__STATIC_INLINE uint32_t Comp_1_GetStatus(void)
{
    return Cy_CTB_CompGetStatus(Comp_1_CTB_HW, Comp_1_COMP_NUM);
}

/*******************************************************************************
* Function Name: Comp_1_EnableInterrupt
****************************************************************************//**
*
* Enable comparator interrupts. Additionally, for interrupts to work,
* the interrupt edge type should not be set to disabled
* in the customizer or with Comp_1_SetInterruptEdgeType().
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_EnableInterrupt(void)
{
    /* Make sure not to disturb the other comparator in the CTB. */
    Comp_1_CTB_HW->INTR_MASK |= (uint32_t) Comp_1_COMP_NUM;
}

/*******************************************************************************
* Function Name: Comp_1_DisableInterrupt
****************************************************************************//**
*
* Disable comparator interrupts.
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_DisableInterrupt(void)
{
    /* Make sure not to disturb the other comparator in the CTB. */
    Comp_1_CTB_HW->INTR_MASK &= ~((uint32_t) Comp_1_COMP_NUM);
}

/*******************************************************************************
* Function Name: Comp_1_GetInterruptStatus
****************************************************************************//**
*
* Return the status of the interrupt.
*
* \return Interrupt status
* - 0: Edge was not detected
* - Non-zero: Edge was detected
*
*******************************************************************************/
__STATIC_INLINE uint32_t Comp_1_GetInterruptStatus(void)
{
    return Cy_CTB_GetInterruptStatus(Comp_1_CTB_HW, Comp_1_COMP_NUM);
}

/*******************************************************************************
* Function Name: Comp_1_ClearInterrupt
****************************************************************************//**
*
* Clear the comparator interrupt.
* The interrupt must be cleared with this function so that the hardware
* can set subsequent interrupts and those interrupts can be forwarded
* to the interrupt controller, if enabled.
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_ClearInterrupt(void)
{
    Cy_CTB_ClearInterrupt(Comp_1_CTB_HW, Comp_1_COMP_NUM);
}

/*******************************************************************************
* Function Name: Comp_1_SetInterrupt
****************************************************************************//**
*
* Force the comparator interrupt to trigger using software.
*
*******************************************************************************/
__STATIC_INLINE void Comp_1_SetInterrupt(void)
{
    Cy_CTB_SetInterrupt(Comp_1_CTB_HW, Comp_1_COMP_NUM);
}

/*******************************************************************************
* Function Name: Comp_1_GetInterruptMask
****************************************************************************//**
*
* Return whether interrupts are enabled.
*
* \return Interrupt mask
* - 0: Interrupt not enabled
* - Non-zero: Interrupt enabled
*
*******************************************************************************/
__STATIC_INLINE uint32_t Comp_1_GetInterruptMask(void)
{
    return Cy_CTB_GetInterruptMask(Comp_1_CTB_HW, Comp_1_COMP_NUM);
}

/*******************************************************************************
* Function Name: Comp_1_GetInterruptStatusMasked
****************************************************************************//**
*
* Return the comparator interrupt status after being masked.
* This is a bitwise AND of Comp_1_GetInterruptStatus() and
* Comp_1_GetInterruptMask().
*
* \return Interrupt mask
* - 0: Edge not detected or interrupt not enabled
* - Non-zero: Edge detected and interrupt enabled
*
*******************************************************************************/
__STATIC_INLINE uint32_t Comp_1_GetInterruptStatusMasked(void)
{
    return Cy_CTB_GetInterruptStatusMasked(Comp_1_CTB_HW, Comp_1_COMP_NUM);
}

#endif /* !defined(CY_Comp_1_H) */


/* [] END OF FILE */
