/*******************************************************************************
* File Name: contatore.h
* Version 1.0
*
* Description:
*  This file provides constants and parameter values for the contatore
*  component.
*
********************************************************************************
* Copyright 2016-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(contatore_CY_TCPWM_COUNTER_PDL_H)
#define contatore_CY_TCPWM_COUNTER_PDL_H

#include "cyfitter.h"
#include "tcpwm/cy_tcpwm_counter.h"

   
/*******************************************************************************
* Variables
*******************************************************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t  contatore_initVar;
extern cy_stc_tcpwm_counter_config_t const contatore_config;
/** @} group_globals */


/***************************************
*  Conditional Compilation Parameters
***************************************/

#define contatore_INIT_COMPARE_OR_CAPTURE    (2UL)


/***************************************
*        Function Prototypes
****************************************/
/**
* \addtogroup group_general
* @{
*/
void contatore_Start(void);
__STATIC_INLINE cy_en_tcpwm_status_t contatore_Init(cy_stc_tcpwm_counter_config_t const *config);
__STATIC_INLINE void contatore_DeInit(void);
__STATIC_INLINE void contatore_Enable(void);
__STATIC_INLINE void contatore_Disable(void);
__STATIC_INLINE uint32_t contatore_GetStatus(void);

#if(CY_TCPWM_COUNTER_MODE_CAPTURE == contatore_INIT_COMPARE_OR_CAPTURE)
    __STATIC_INLINE uint32_t contatore_GetCapture(void);
    __STATIC_INLINE uint32_t contatore_GetCaptureBuf(void);
#else
    __STATIC_INLINE void contatore_SetCompare0(uint32_t compare0);
    __STATIC_INLINE uint32_t contatore_GetCompare0(void);
    __STATIC_INLINE void contatore_SetCompare1(uint32_t compare1);
    __STATIC_INLINE uint32_t contatore_GetCompare1(void);
    __STATIC_INLINE void contatore_EnableCompareSwap(bool enable);
#endif /* (CY_TCPWM_COUNTER_MODE_CAPTURE == contatore_INIT_COMPARE_OR_CAPTURE) */

__STATIC_INLINE void contatore_SetCounter(uint32_t count);
__STATIC_INLINE uint32_t contatore_GetCounter(void);
__STATIC_INLINE void contatore_SetPeriod(uint32_t period);
__STATIC_INLINE uint32_t contatore_GetPeriod(void);
__STATIC_INLINE void contatore_TriggerStart(void);
__STATIC_INLINE void contatore_TriggerReload(void);
__STATIC_INLINE void contatore_TriggerStop(void);
__STATIC_INLINE void contatore_TriggerCapture(void);
__STATIC_INLINE uint32_t contatore_GetInterruptStatus(void);
__STATIC_INLINE void contatore_ClearInterrupt(uint32_t source);
__STATIC_INLINE void contatore_SetInterrupt(uint32_t source);
__STATIC_INLINE void contatore_SetInterruptMask(uint32_t mask);
__STATIC_INLINE uint32_t contatore_GetInterruptMask(void);
__STATIC_INLINE uint32_t contatore_GetInterruptStatusMasked(void);
/** @} general */


/***************************************
*           API Constants
***************************************/

/**
* \addtogroup group_macros
* @{
*/
/** This is a ptr to the base address of the TCPWM instance */
#define contatore_HW                 (contatore_TCPWM__HW)

/** This is a ptr to the base address of the TCPWM CNT instance */
#define contatore_CNT_HW             (contatore_TCPWM__CNT_HW)

/** This is the counter instance number in the selected TCPWM */
#define contatore_CNT_NUM            (contatore_TCPWM__CNT_IDX)

/** This is the bit field representing the counter instance in the selected TCPWM */
#define contatore_CNT_MASK           (1UL << contatore_CNT_NUM)
/** @} group_macros */

#define contatore_INPUT_MODE_MASK    (0x3U)
#define contatore_INPUT_DISABLED     (7U)


/*******************************************************************************
* Function Name: contatore_Init
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_tcpwm_status_t contatore_Init(cy_stc_tcpwm_counter_config_t const *config)
{
    return(Cy_TCPWM_Counter_Init(contatore_HW, contatore_CNT_NUM, config));
}


/*******************************************************************************
* Function Name: contatore_DeInit
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_DeInit(void)                   
{
    Cy_TCPWM_Counter_DeInit(contatore_HW, contatore_CNT_NUM, &contatore_config);
}

/*******************************************************************************
* Function Name: contatore_Enable
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Enable_Multiple() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_Enable(void)                   
{
    Cy_TCPWM_Enable_Multiple(contatore_HW, contatore_CNT_MASK);
}


/*******************************************************************************
* Function Name: contatore_Disable
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Disable_Multiple() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_Disable(void)                  
{
    Cy_TCPWM_Disable_Multiple(contatore_HW, contatore_CNT_MASK);
}


/*******************************************************************************
* Function Name: contatore_GetStatus
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_GetStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t contatore_GetStatus(void)                
{
    return(Cy_TCPWM_Counter_GetStatus(contatore_HW, contatore_CNT_NUM));
}


#if(CY_TCPWM_COUNTER_MODE_CAPTURE == contatore_INIT_COMPARE_OR_CAPTURE)
    /*******************************************************************************
    * Function Name: contatore_GetCapture
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_GetCapture() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE uint32_t contatore_GetCapture(void)               
    {
        return(Cy_TCPWM_Counter_GetCapture(contatore_HW, contatore_CNT_NUM));
    }


    /*******************************************************************************
    * Function Name: contatore_GetCaptureBuf
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_GetCaptureBuf() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE uint32_t contatore_GetCaptureBuf(void)            
    {
        return(Cy_TCPWM_Counter_GetCaptureBuf(contatore_HW, contatore_CNT_NUM));
    }
#else
    /*******************************************************************************
    * Function Name: contatore_SetCompare0
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_SetCompare0() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE void contatore_SetCompare0(uint32_t compare0)      
    {
        Cy_TCPWM_Counter_SetCompare0(contatore_HW, contatore_CNT_NUM, compare0);
    }


    /*******************************************************************************
    * Function Name: contatore_GetCompare0
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_GetCompare0() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE uint32_t contatore_GetCompare0(void)              
    {
        return(Cy_TCPWM_Counter_GetCompare0(contatore_HW, contatore_CNT_NUM));
    }


    /*******************************************************************************
    * Function Name: contatore_SetCompare1
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_SetCompare1() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE void contatore_SetCompare1(uint32_t compare1)      
    {
        Cy_TCPWM_Counter_SetCompare1(contatore_HW, contatore_CNT_NUM, compare1);
    }


    /*******************************************************************************
    * Function Name: contatore_GetCompare1
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_GetCompare1() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE uint32_t contatore_GetCompare1(void)              
    {
        return(Cy_TCPWM_Counter_GetCompare1(contatore_HW, contatore_CNT_NUM));
    }


    /*******************************************************************************
    * Function Name: contatore_EnableCompareSwap
    ****************************************************************************//**
    *
    * Invokes the Cy_TCPWM_Counter_EnableCompareSwap() PDL driver function.
    *
    *******************************************************************************/
    __STATIC_INLINE void contatore_EnableCompareSwap(bool enable)  
    {
        Cy_TCPWM_Counter_EnableCompareSwap(contatore_HW, contatore_CNT_NUM, enable);
    }
#endif /* (CY_TCPWM_COUNTER_MODE_CAPTURE == contatore_INIT_COMPARE_OR_CAPTURE) */


/*******************************************************************************
* Function Name: contatore_SetCounter
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_SetCounter() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_SetCounter(uint32_t count)          
{
    Cy_TCPWM_Counter_SetCounter(contatore_HW, contatore_CNT_NUM, count);
}


/*******************************************************************************
* Function Name: contatore_GetCounter
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_GetCounter() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t contatore_GetCounter(void)               
{
    return(Cy_TCPWM_Counter_GetCounter(contatore_HW, contatore_CNT_NUM));
}


/*******************************************************************************
* Function Name: contatore_SetPeriod
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_SetPeriod() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_SetPeriod(uint32_t period)          
{
    Cy_TCPWM_Counter_SetPeriod(contatore_HW, contatore_CNT_NUM, period);
}


/*******************************************************************************
* Function Name: contatore_GetPeriod
****************************************************************************//**
*
* Invokes the Cy_TCPWM_Counter_GetPeriod() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t contatore_GetPeriod(void)                
{
    return(Cy_TCPWM_Counter_GetPeriod(contatore_HW, contatore_CNT_NUM));
}


/*******************************************************************************
* Function Name: contatore_TriggerStart
****************************************************************************//**
*
* Invokes the Cy_TCPWM_TriggerStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_TriggerStart(void)             
{
    Cy_TCPWM_TriggerStart(contatore_HW, contatore_CNT_MASK);
}


/*******************************************************************************
* Function Name: contatore_TriggerReload
****************************************************************************//**
*
* Invokes the Cy_TCPWM_TriggerReloadOrIndex() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_TriggerReload(void)     
{
    Cy_TCPWM_TriggerReloadOrIndex(contatore_HW, contatore_CNT_MASK);
}


/*******************************************************************************
* Function Name: contatore_TriggerStop
****************************************************************************//**
*
* Invokes the Cy_TCPWM_TriggerStopOrKill() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_TriggerStop(void)
{
    Cy_TCPWM_TriggerStopOrKill(contatore_HW, contatore_CNT_MASK);
}


/*******************************************************************************
* Function Name: contatore_TriggerCapture
****************************************************************************//**
*
* Invokes the Cy_TCPWM_TriggerCaptureOrSwap() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_TriggerCapture(void)     
{
    Cy_TCPWM_TriggerCaptureOrSwap(contatore_HW, contatore_CNT_MASK);
}


/*******************************************************************************
* Function Name: contatore_GetInterruptStatus
****************************************************************************//**
*
* Invokes the Cy_TCPWM_GetInterruptStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t contatore_GetInterruptStatus(void)       
{
    return(Cy_TCPWM_GetInterruptStatus(contatore_HW, contatore_CNT_NUM));
}


/*******************************************************************************
* Function Name: contatore_ClearInterrupt
****************************************************************************//**
*
* Invokes the Cy_TCPWM_ClearInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_ClearInterrupt(uint32_t source)     
{
    Cy_TCPWM_ClearInterrupt(contatore_HW, contatore_CNT_NUM, source);
}


/*******************************************************************************
* Function Name: contatore_SetInterrupt
****************************************************************************//**
*
* Invokes the Cy_TCPWM_SetInterrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_SetInterrupt(uint32_t source)
{
    Cy_TCPWM_SetInterrupt(contatore_HW, contatore_CNT_NUM, source);
}


/*******************************************************************************
* Function Name: contatore_SetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_TCPWM_SetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void contatore_SetInterruptMask(uint32_t mask)     
{
    Cy_TCPWM_SetInterruptMask(contatore_HW, contatore_CNT_NUM, mask);
}


/*******************************************************************************
* Function Name: contatore_GetInterruptMask
****************************************************************************//**
*
* Invokes the Cy_TCPWM_GetInterruptMask() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t contatore_GetInterruptMask(void)         
{
    return(Cy_TCPWM_GetInterruptMask(contatore_HW, contatore_CNT_NUM));
}


/*******************************************************************************
* Function Name: contatore_GetInterruptStatusMasked
****************************************************************************//**
*
* Invokes the Cy_TCPWM_GetInterruptStatusMasked() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t contatore_GetInterruptStatusMasked(void)
{
    return(Cy_TCPWM_GetInterruptStatusMasked(contatore_HW, contatore_CNT_NUM));
}

#endif /* contatore_CY_TCPWM_COUNTER_PDL_H */


/* [] END OF FILE */
