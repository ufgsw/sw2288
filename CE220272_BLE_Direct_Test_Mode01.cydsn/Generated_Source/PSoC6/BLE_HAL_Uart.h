/***************************************************************************//**
* \file BLE_HAL_Uart.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(BLE_HAL_Uart_CY_SCB_UART_PDL_H)
#define BLE_HAL_Uart_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define BLE_HAL_Uart_DIRECTION  (3U)
#define BLE_HAL_Uart_ENABLE_RTS (0U)
#define BLE_HAL_Uart_ENABLE_CTS (0U)

/* UART direction enum */
#define BLE_HAL_Uart_RX    (0x1U)
#define BLE_HAL_Uart_TX    (0x2U)

#define BLE_HAL_Uart_ENABLE_RX  (0UL != (BLE_HAL_Uart_DIRECTION & BLE_HAL_Uart_RX))
#define BLE_HAL_Uart_ENABLE_TX  (0UL != (BLE_HAL_Uart_DIRECTION & BLE_HAL_Uart_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void BLE_HAL_Uart_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t BLE_HAL_Uart_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void BLE_HAL_Uart_DeInit(void);
__STATIC_INLINE void BLE_HAL_Uart_Enable(void);
__STATIC_INLINE void BLE_HAL_Uart_Disable(void);

/* Register callback. */
__STATIC_INLINE void BLE_HAL_Uart_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (BLE_HAL_Uart_ENABLE_CTS)
__STATIC_INLINE void BLE_HAL_Uart_EnableCts(void);
__STATIC_INLINE void BLE_HAL_Uart_DisableCts(void);
#endif /* (BLE_HAL_Uart_ENABLE_CTS) */

#if (BLE_HAL_Uart_ENABLE_RTS)
__STATIC_INLINE void     BLE_HAL_Uart_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetRtsFifoLevel(void);
#endif /* (BLE_HAL_Uart_ENABLE_RTS) */

__STATIC_INLINE void BLE_HAL_Uart_EnableSkipStart(void);
__STATIC_INLINE void BLE_HAL_Uart_DisableSkipStart(void);

#if (BLE_HAL_Uart_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t BLE_HAL_Uart_Get(void);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     BLE_HAL_Uart_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetRxFifoStatus(void);
__STATIC_INLINE void     BLE_HAL_Uart_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumInRxFifo(void);
__STATIC_INLINE void     BLE_HAL_Uart_ClearRxFifo(void);
#endif /* (BLE_HAL_Uart_ENABLE_RX) */

#if (BLE_HAL_Uart_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t BLE_HAL_Uart_Put(uint32_t data);
__STATIC_INLINE uint32_t BLE_HAL_Uart_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     BLE_HAL_Uart_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     BLE_HAL_Uart_PutString(char_t const string[]);
__STATIC_INLINE void     BLE_HAL_Uart_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetTxFifoStatus(void);
__STATIC_INLINE void     BLE_HAL_Uart_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumInTxFifo(void);
__STATIC_INLINE bool     BLE_HAL_Uart_IsTxComplete(void);
__STATIC_INLINE void     BLE_HAL_Uart_ClearTxFifo(void);
#endif /* (BLE_HAL_Uart_ENABLE_TX) */

#if (BLE_HAL_Uart_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     BLE_HAL_Uart_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     BLE_HAL_Uart_StopRingBuffer(void);
__STATIC_INLINE void     BLE_HAL_Uart_ClearRingBuffer(void);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t BLE_HAL_Uart_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     BLE_HAL_Uart_AbortReceive(void);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetReceiveStatus(void);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumReceived(void);
#endif /* (BLE_HAL_Uart_ENABLE_RX) */

#if (BLE_HAL_Uart_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t BLE_HAL_Uart_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     BLE_HAL_Uart_AbortTransmit(void);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetTransmitStatus(void);
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumLeftToTransmit(void);
#endif /* (BLE_HAL_Uart_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void BLE_HAL_Uart_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t BLE_HAL_Uart_initVar;
extern cy_stc_scb_uart_config_t const BLE_HAL_Uart_config;
extern cy_stc_scb_uart_context_t BLE_HAL_Uart_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define BLE_HAL_Uart_HW     ((CySCB_Type *) BLE_HAL_Uart_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: BLE_HAL_Uart_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t BLE_HAL_Uart_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(BLE_HAL_Uart_HW, config, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_DeInit(void)
{
    Cy_SCB_UART_DeInit(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_Enable(void)
{
    Cy_SCB_UART_Enable(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_Disable(void)
{
    Cy_SCB_UART_Disable(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(BLE_HAL_Uart_HW, callback, &BLE_HAL_Uart_context);
}


#if (BLE_HAL_Uart_ENABLE_CTS)
/*******************************************************************************
* Function Name: BLE_HAL_Uart_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(BLE_HAL_Uart_HW);
}
#endif /* (BLE_HAL_Uart_ENABLE_CTS) */


#if (BLE_HAL_Uart_ENABLE_RTS)
/*******************************************************************************
* Function Name: BLE_HAL_Uart_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(BLE_HAL_Uart_HW, level);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(BLE_HAL_Uart_HW);
}
#endif /* (BLE_HAL_Uart_ENABLE_RTS) */


/*******************************************************************************
* Function Name: BLE_HAL_Uart_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(BLE_HAL_Uart_HW);
}


#if (BLE_HAL_Uart_ENABLE_RX)
/*******************************************************************************
* Function Name: BLE_HAL_Uart_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_Get(void)
{
    return Cy_SCB_UART_Get(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(BLE_HAL_Uart_HW, buffer, size);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(BLE_HAL_Uart_HW, buffer, size);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(BLE_HAL_Uart_HW, clearMask);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(BLE_HAL_Uart_HW);
}
#endif /* (BLE_HAL_Uart_ENABLE_RX) */


#if (BLE_HAL_Uart_ENABLE_TX)
/*******************************************************************************
* Function Name: BLE_HAL_Uart_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(BLE_HAL_Uart_HW,data);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(BLE_HAL_Uart_HW, buffer, size);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(BLE_HAL_Uart_HW, buffer, size);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(BLE_HAL_Uart_HW, string);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(BLE_HAL_Uart_HW, breakWidth);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(BLE_HAL_Uart_HW, clearMask);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool BLE_HAL_Uart_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(BLE_HAL_Uart_HW);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(BLE_HAL_Uart_HW);
}
#endif /* (BLE_HAL_Uart_ENABLE_TX) */


#if (BLE_HAL_Uart_ENABLE_RX)
/*******************************************************************************
* Function Name: BLE_HAL_Uart_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(BLE_HAL_Uart_HW, buffer, size, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t BLE_HAL_Uart_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(BLE_HAL_Uart_HW, buffer, size, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}
#endif /* (BLE_HAL_Uart_ENABLE_RX) */


#if (BLE_HAL_Uart_ENABLE_TX)
/*******************************************************************************
* Function Name: BLE_HAL_Uart_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t BLE_HAL_Uart_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(BLE_HAL_Uart_HW, buffer, size, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}


/*******************************************************************************
* Function Name: BLE_HAL_Uart_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t BLE_HAL_Uart_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}
#endif /* (BLE_HAL_Uart_ENABLE_TX) */


/*******************************************************************************
* Function Name: BLE_HAL_Uart_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void BLE_HAL_Uart_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(BLE_HAL_Uart_HW, &BLE_HAL_Uart_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* BLE_HAL_Uart_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
