/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "utenti.h"

// Release
#define RELEASE_FW     121
#define RELEASE_HW     100
#define RELEASE_PROTO    2
#define RELEASE_PAR    100
#define RELEASE_UTENTI 100
#define RELEASE_LOG    100
#define RELEASE_CRYPTO 0xa3
#define LED_ACCESO       0
#define LED_SPENTO       1

#define DSMART           1  // Serratura BLE
#define EASY_GATE        2  // Apertura tramite tastiera, tag o fingerprint
#define OPEN_AIR         3  // Apertura solo da remoto tramite Bridge

// #define TEST_APERTURE    // La porta si aprirà e chiuderà ogni minuto per poter testare la durata della batteria

typedef enum
{
   LED_OFF     = 0,
   LED_VERDE   = 0x20,
   LED_ROSSO   = 0x40,
   LED_ARANCIO = 0x60
}eLeds;

typedef enum
{
    TASTO_NULL = ' ',
    TASTO_0    = '0', 
    TASTO_1    = '1',
    TASTO_2    = '2',
    TASTO_3    = '3',
    TASTO_4    = '4',
    TASTO_5    = '5',
    TASTO_6    = '6',
    TASTO_7    = '7',
    TASTO_8    = '8',
    TASTO_9    = '9',
    TASTO_E    = 'E',
    TASTO_R    = 'R'
}eTastiera;

typedef enum
{
   exe_init = 0,
   exe_null,
   exe_run,
   exe_add_rfid,
   exe_add_fingerprint,
   exe_add_bridge,
   exe_disable_crypto,
   exe_recovery,
   exe_enter_sleep,
   exe_deepsleep,
   exe_wakeup,
   exe_disable,
   exe_power_off,
   exe_err
}eExe;

typedef struct
{
    unsigned init_ok          :1;
    
    unsigned send_masterok    :1;
    
    unsigned apri_porta_k1    :1;
    unsigned apri_porta_k2    :1;
    unsigned apri_porta_k3    :1;
    
    unsigned apri_da_citofono :1;
    //unsigned chiudi_porta     :1;
    unsigned read_tastiera    :1;
    unsigned led_on           :1;

    unsigned calibra_porta_chiusa     :1;
    unsigned calibra_serratura_chiusa :1;

    unsigned calibra_corsa    :1;                   // 10
    unsigned calibra_batteria :1;

    unsigned tasto_aprichiudi :1;
    unsigned tasto_premuto    :1;
    unsigned tasto_rilasciato :1;
    unsigned tasto_e          :1;
    unsigned tasto_r          :1;
    unsigned tasto_err        :1;

    unsigned associa_rfid        :1;
    unsigned associa_fingerprint :1;
    unsigned dito_appoggiato     :1;
    unsigned elimina_tutte_impronte :1;
    unsigned impronta_letta :1;
    
    unsigned codice_rfid      :1;                   // 20
    unsigned codice_err       :1;
    unsigned in_sleep         :1;
    unsigned nfc_power_down   :1;
    unsigned wakeup           :1;

    unsigned notifica       :1;

    unsigned ble_connect           :1;
    unsigned ble_update_adv        :1;
    unsigned ble_accoppia_telefono :1;
    unsigned ble_accoppia_tastiera :1;
    unsigned ble_accoppia_bridge   :1;              // 30

    unsigned crypto_en   :1;

    unsigned save_par            :1;
    unsigned save_utenti         :1;
    unsigned save_log            :1;
    unsigned save_orari_chiusura :1;
    unsigned save_logufg         :1;   

    unsigned cronometro :1;

    unsigned recovery     :1;
    unsigned reset_scheda :1;
    unsigned upgrade_fw   :1;                       // 40
    unsigned start_boot   :1;

    unsigned send_event_tocloud  :1;
    unsigned send_users_tocloud :1;
    unsigned send_user_tocloud :1;
    unsigned send_par_tocloud :1;
    unsigned send_all_tocloud :1;
    unsigned send_wifi_tobridge :1;

    unsigned serratura_disabilitata :1;    
    unsigned finger_disabilitato :1;
 
}sFlag;
extern sFlag flag;

typedef struct
{
    unsigned nuova             :1;
    unsigned abbina_tastiera   :1;
    unsigned abbina_telefono   :1;
    unsigned disabilitata      :1;
    unsigned recovery          :1;
    unsigned abbina_bridge     :1;
    unsigned abbina_impronta   :1;
    unsigned nuovo_evento      :1;
}eAdv;

typedef union
{   
   eAdv    type;
   uint8_t val;
}uAdv;
extern uAdv adv;

typedef enum
{
   mot_init = 0,
   mot_attesa,
   mot_attesa_rilascio_tasto,
   mot_attesa_porta_chiusa,
   mot_apri,
   mot_chiudi,
   mot_in_apertura,
   mot_tenuta_scrocchio,
   mot_in_apertura_a_tempo,
   mot_in_chiusura_a_tempo,
   mot_in_chiusura_limitazione,
   mot_uscita_scrocchio,
   mot_attesa_uscita_scrocchio,
   
   mot_calibra_apri,
   mot_calibra_apri_attesa,
   mot_calibra_chiudi,
   mot_calibra_chiudi_attesa,
   
   mot_fine_movimento    
}eMotore;

typedef enum
{
    chiusura_con_limitazione = 0,
    chiusura_a_tempo         = 1
}eTipoMovimentazioneChiusura;

typedef enum
{
    chiusura_manuale       = 0,
    chiusura_semiatomatica = 1,
    chiusura_automatica    = 2
}eTipoChiusura;

typedef enum
{
    apertura_con_scrocchio   = 0,
    apertura_senza_scrocchio = 1,   
}eTipoApertura;

typedef enum
{
   uso_niente          = 0,
   uso_magnetometro    = 1,
   uso_sensore_esterno = 2
}eTipoSensore;

typedef enum
{
   alimentazione_batteria = 0,
   alimentazione_rete     = 1
}eTipoAlimentazione;

typedef enum
{
   apertura_destra   = 0,
   apertura_sinistra = 1
}eDirezioneApertura;

typedef enum
{
   leds_off          = 0,
   led_verde_on      = 1,
   led_verde_toggle  = 2,
   led_rosso_on      = 3,
   led_rosso_toggle  = 4,
   led_rosso_toggle5 = 5,
   led_blu_on        = 6,
   led_blu_toggle    = 7,
   led_bianco_on     = 8,
   led_bianco_toggle = 9
}eLed;

typedef struct 
{
   unsigned mbr3         :1;
   unsigned fram         :1;
   unsigned magnetometro :1;
   unsigned ble_hw       :1;
   unsigned wco          :1;
   unsigned opt          :3;
}sErrFlag;

typedef union 
{
    uint8_t    val;
    sErrFlag   bit;    
}uErr;
extern uErr  error;

typedef struct 
{
   unsigned ingresso_k1 :1;
   unsigned ingresso_k2 :1;
   unsigned ingresso_k3 :1;
   unsigned opt         :5;
}sStatoIngressi;

typedef union
{
   uint8_t     val;
   sStatoIngressi bit;
}uStatoIngressi;
extern uStatoIngressi statoIngressi;

typedef enum
{
   send_null = 0,
   send_par1,
   send_users,
   send_user, 
   send_chiusure,
   send_sensors,
   send_sensors_cont,
   send_nomi_rele,
    send_log,
   send_log_serratura,
   send_log_ufg,
   send_orologio,
   send_crypto_key,
   send_random_number,
   send_test,
   send_evento,
   send_rfid_ok,
   send_rfid_fail,
   send_end,
   send_error_code,
   send_bdaddress,
   send_bdaddress_in_chiaro,
   send_bridgeid,
   send_utente_mask,
   send_user_tocloud,
   send_par_tocloud,
   send_backdoor,
   send_wifi_par
}eFaseSend;
//extern eFaseSend faseSend;

typedef enum
{
    cmd_write_par = 0x01,
    cmd_edit_user = 0x02,
    cmd_add_user  = 0x03,
    cmd_del_user = 0x04,
    cmd_set_clock = 0x06,
    cmd_link_rfid_user = 0x07,
    cmd_link_finger_user = 0x08,
    cmd_del_user_affitto = 0x09,
    cmd_send_log_user = 0x05,
    cmd_send_log_serratura = 0x35,
    cmd_send_log_ufg = 0x34,
    cmd_del_log_ufg = 0x34,
    cmd_del_log = 0x13,
    cmd_programmed_closed = 0x36,
    cmd_edit_date_time = 0x06,
    cmd_get_date_time = 0x16,
    cmd_master = 0x10,
    cmd_open_door = 0x11,
    cmd_close_door = 0x12,
    cmd_get_params = 0x14,
    cmd_edit_sensors = 0x15,
    cmd_save_status_close_door = 0x17,
    cmd_edit_name_door = 0x18,
    cmd_calibration_lock = 0x19,
    cmd_edit_psw_master = 0x1a,
    cmd_exe_pair_device = 0x1b,
    cmd_get_random_number = 0x1c,
    cmd_edit_enable_users = 0x1d,
    cmd_reset_default = 0x1e,
    cmd_edit_oldnew_door = 0x1f,
    cmd_calibration_battery = 0x30,
    cmd_get_crypto_message_test = 0x31,
    cmd_edit_serialnumber = 0x32,
    cmd_set_led_on = 0x33,
    cmd_set_out_on = 0x34,
    cmd_get_aes_key = 0x20,
    cmd_add_userweb = 0x21,
    cmd_edit_enable_send_aeskey = 0x22,
    cmd_send_test_message = 0x23,
    cmd_send_new_firmware = 0x50,
    cmd_sen_errcode = 0xf0,
    cmd_end_trasmission = 0xff,
}eCommand;

typedef struct
{
    cy_stc_ble_conn_handle_t            connHandle;
    cy_stc_ble_gatt_handle_value_pair_t notificationHandle;
    eFaseSend                           faseSend;
    eFaseSend                           faseSendNext;
    eErrorCode                          errCode;
    eCommand                            errCommand;
    int cUtente;
    int timeout;
    
    uint8_t is_bridge;
    uint8_t master_ok;
    uint8_t bdAddress[6];
    
}eGapDevice;
extern eGapDevice gapDevice[CY_BLE_MAX_CONNECTION_INSTANCES];

typedef enum
{
   out_attivo_basso = 0,
   out_attivo_alto  = 1   
}eOutStatoAttivo;

//typedef enum
//{
//   out_prima_di_aprire   = 1,
//   out_dopo_scrocchio    = 2,
//   out_dopo_apertura     = 3,
//   out_prima_di_chiudere = 4,
//   out_dopo_chiusura     = 5,
//   
//   out_sensore_porta_chiusa = 6,
//   out_sensore_porta_aperta = 7
//   
//}eOutQuandoAttivo;

typedef struct
{
   uint8_t          anable;
   eOutStatoAttivo  stato_attivo;     // attivo alto o basso
   uint8_t          s_ritardo;        // ritarda l'accensione di x decimi di secondo 
   uint16_t         ms_attivo;        // ms di abilitazione   
}sProgrammableOutput;

typedef struct
{
   int x;
   int y;
   int z;
}sMagnete;

typedef struct
{
    uint8_t  release;
    uint8_t  nuova;

    uint8_t  serial_number[6];
    uint8_t  localname[10];

    uint8_t  timezone;

    eTipoSensore       tipo_sensore_chiusura;
    eTipoAlimentazione tipo_alimentazione;
    eDirezioneApertura direzione_apertura;
    eTipoChiusura      tipo_chiusura;

    sProgrammableOutput output;
    sProgrammableOutput rele1;
    sProgrammableOutput rele2;

    uint8_t  tempo_massimo_porta_aperta; // in secondi

    int16_t  livello_batteria_3v7;
    uint8_t  enable_citofono;
    uint8_t  en_tastiera_tag;
    uint8_t  en_buzzer_ext;

    uint8_t  id_fingerprint[8];
    
    uint8_t  nome_rele1[20];
    uint8_t  nome_rele2[20];
    uint8_t  nome_out_rogrammabile[20];
    
    uint8_t mask_sensore1;
    uint8_t mask_sensore2;
    
    uint8_t opt[8];

    uint8_t  crc;   
}sParametri;

typedef struct
{
   uint8_t plog;
   uint8_t tlog;
   sLog    log[MAX_LOG];
}sLogSerratura;



typedef struct
{
   uint8_t enable;
   uint8_t ore;
   uint8_t minuti;
}sOrarioChiusura;

#define RESET_DA_JUMPER 1
#define RESET_DA_BLE    2
#define RESET_DA_NUOVA  3
#define MAX_CONN_HANDLE 4
typedef struct
{
   int8_t release;
   
   // Salvo l'inizio del conteggio aperture/chiusure
   int8_t giorno_inizio;
   int8_t mese_inizio;
   int8_t anno_inizio;
   
   // Salvo tutte le movimentazioni del motore
   int32_t totale_aperture;
   int32_t totale_chiusure;
   
   // Salvo quanto tempo è stato connesso il BLE
   int32_t secondi_ble;

   // Salvo quando è stata creata l'ultima chiave cittografica ( reset dati default )
   int8_t giorno_eas;
   int8_t mese_eas;
   int8_t anno_eas;
   
   int8_t evento_reset; // 1 = reset da jumper, 2 = reset da smartphone
      
}sLogUfg;

typedef struct
{
    uint8_t id[20];
    uint8_t bdaddress[6];
    uint8_t accoppiato;
    uint8_t optional[5];
}bridge_s;
extern bridge_s bridge;

typedef struct
{
    uint8_t ssid[50];
    uint8_t len_ssid;
    uint8_t pass[50];
    uint8_t len_pass;
    uint8_t channel;
}wifi_s;

extern sLogSerratura logSerratura;

extern sParametri par;
extern uint8_t    batteria;
extern uint8_t    batteria_old;
extern uint8_t    codice_rfid[10];
extern uint32_t   timeout_ble;
extern uint32_t   timeout_mastercode;
extern uint8_t    revision;
extern uint8_t    codice_impronta;
extern uint8_t    bdAddress[6];                      
extern uint8_t    tipo_scheda;

void    BLE_Data();
uint8_t writePar( uint8_t *mes, int len ,cy_stc_ble_conn_handle_t connHandle);
void    setLed(eLed led);

void applicationTask();
void isrTasto();
void isrCitofono();
void isrCapsense();
void isrPort10();
void tick_1ms();
void isr_RTC();
//void invia_a_tastiera_esterna( int b1, int b2, int b3 );
void set_tastiera_esterna( int b1, int b2, int b3 , int b4, int b5);
void test_setLed(eLed led);

/* [] END OF FILE */
