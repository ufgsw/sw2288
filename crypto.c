/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include "project.h"
#include "crypto.h"

cy_en_crypto_status_t           cryptoStatus;
cy_stc_crypto_server_context_t  myCryptoServerContext;
cy_stc_crypto_context_t         myCryptoContext;

cy_stc_crypto_context_rsa_t     cryptoRsaContext;
cy_stc_crypto_context_rsa_ver_t cryptoRsaVerContext;
cy_en_crypto_rsa_ver_result_t   verResult;

cy_stc_crypto_context_des_t     cryptoDesContext;

cy_stc_crypto_context_aes_t     cryptoAesContext;
cy_stc_crypto_context_t         cryptoScratch;
cy_stc_crypto_context_aes_t     cryptoAES;

cy_stc_syspm_callback_params_t  callbackParams;

sCryptoKey crypto;

//cy_stc_crypto_rsa_pub_key_t publicKeyStruct;
//cy_stc_crypto_rsa_pub_key_t privateKeyStruct;
//
//CY_ALIGN(4) uint8_t modus[32] =
//{
//   0x80,0x9e,0x38,0x9c,0x43,0xc7,0xf4,0x37,
//   0x13,0x39,0xd7,0xc7,0xe0,0xa8,0x0d,0x22,
//   0xf1,0x7d,0xa2,0x1c,0x87,0x03,0xcc,0x31,
//   0xd9,0x7a,0xb1,0xf3,0x44,0x92,0xf5,0xf9
//};
//
//CY_ALIGN(4) uint8_t private_exp[32] =
//{
//   0x24,0x30,0xe1,0x4b,0x0f,0x43,0x8f,0x73,
//   0x83,0xe4,0x6d,0xd4,0x5f,0x61,0xca,0x38,
//   0x17,0xd6,0x9a,0x39,0xca,0x66,0x4c,0xdd,
//   0x7e,0x1b,0xbd,0xa5,0x2a,0x92,0xd1,0xc5
//};
//
//CY_ALIGN(4) uint8_t public_exp[32] =
//{
//   0x01, 0x00, 0x01
//};

//
///* All data arrays should be 4-byte aligned */
//cy_stc_public_key_t publicKey =
//{
//  .publicKeyStruct =
//  {
//      .moduloPtr          = 0,
//      .moduloLength       = 256,
//      .pubExpPtr          = 0,
//      .pubExpLength       = 24,
//      .barretCoefPtr      = NULL,
//      .inverseModuloPtr   = NULL,
//      .rBarPtr            = NULL
//  },
//  .moduloData =
//  {   /* modulus in Little Endian for a public key - rsa_public.txt */
//      0xad,0x94,0xba,0x17,0x4c,0x02,0x5c,0x7c,
//      0xa6,0x7c,0x2f,0x76,0x46,0x6a,0xad,0x57,
//      0xf7,0xd2,0xfc,0x2d,0xf4,0xa6,0x22,0x74,
//      0xeb,0x30,0xc2,0x52,0xf1,0x9e,0xc1,0x4b,
//      0xc9,0x40,0xe3,0x33,0x1d,0xbf,0x0d,0x3a,
//      0x50,0xc4,0x1e,0x95,0xe7,0x2b,0x50,0x14,
//      0x13,0xfb,0xbb,0x50,0x24,0x5f,0xdd,0x0b,
//      0x21,0x2e,0x0c,0x72,0x32,0x59,0x92,0x85   
//  },
//  /* Little endian exponent for a public key - rsa_public.txt */
//  .expData = { 0x01, 0x00, 0x01 },//1 00 01
//  .k1Data  = { 0, },
//  .k2Data  = { 0, },
//  .k3Data  = { 0, },
//};
//
//cy_stc_private_key_t privateKey =
//{
//  .privateKeyStruct =
//  {
//      .moduloPtr          = 0,
//      .moduloLength       = 256,
//      .pubExpPtr          = 0,
//      .pubExpLength       = 256,
//      .barretCoefPtr      = NULL,
//      .inverseModuloPtr   = NULL,
//      .rBarPtr            = NULL
//  },
//  .moduloData =
//  {   /* modulus in Little Endian for a public key - rsa_public.txt */
//      0xad,0x94,0xba,0x17,0x4c,0x02,0x5c,0x7c,
//      0xa6,0x7c,0x2f,0x76,0x46,0x6a,0xad,0x57,
//      0xf7,0xd2,0xfc,0x2d,0xf4,0xa6,0x22,0x74,
//      0xeb,0x30,0xc2,0x52,0xf1,0x9e,0xc1,0x4b,
//      0xc9,0x40,0xe3,0x33,0x1d,0xbf,0x0d,0x3a,
//      0x50,0xc4,0x1e,0x95,0xe7,0x2b,0x50,0x14,
//      0x13,0xfb,0xbb,0x50,0x24,0x5f,0xdd,0x0b,
//      0x21,0x2e,0x0c,0x72,0x32,0x59,0x92,0x85   
//  },
//  /* Little endian exponent for a public key - rsa_public.txt */
//  .expData = { 
//      0x78,0x39,0x9a,0xea,0xa7,0xea,0xba,0xc4,
//      0x5b,0x4b,0x88,0x54,0x13,0x51,0xaf,0xde,
//      0x4d,0xf8,0x04,0x52,0x90,0x30,0xc1,0x8c,
//      0xd9,0x99,0x5b,0xfe,0xb6,0xfb,0x8e,0x45,
//      0x18,0xb0,0x23,0xc4,0x0c,0xd6,0xa9,0x24,
//      0x04,0x6a,0xa1,0x3e,0x8a,0x83,0x48,0x28,
//      0xee,0x6d,0x52,0x66,0x6b,0x16,0x2c,0xbc,
//      0x49,0x62,0x59,0x39,0x76,0x59,0x9d,0xc1
//   },
//  .k1Data  = { 0, },
//  .k2Data  = { 0, },
//  .k3Data  = { 0, },
//};

const cy_stc_crypto_config_t myCryptoConfig =
 {
     /* .ipcChannel             */ MY_CHAN_CRYPTO,
     /* .acquireNotifierChannel */ MY_INTR_CRYPTO_SRV,
     /* .releaseNotifierChannel */ MY_INTR_CRYPTO_CLI,
     /* .releaseNotifierConfig */ {
     #if (CY_CPU_CORTEX_M0P)
         /* .intrSrc            */ MY_INTR_CRYPTO_CLI_MUX,
         /* .cm0pSrc            */ cpuss_interrupts_ipc_2_IRQn, /* depends on selected releaseNotifierChannel value */
     #else
         /* .intrSrc            */ cpuss_interrupts_ipc_2_IRQn, /* depends on selected releaseNotifierChannel value */
     #endif
         /* .intrPriority       */ 2u,
     },
     /* .userCompleteCallback   */ NULL,
     /* .userGetDataHandler     */ NULL,
     /* .userErrorHandler       */ NULL,
     /* .acquireNotifierConfig */ {
     #if (CY_CPU_CORTEX_M0P)
         /* .intrSrc            */ MY_INTR_CRYPTO_SRV_MUX,      /* to use with DeepSleep mode should be in DeepSleep capable muxer's range */
         /* .cm0pSrc            */ cpuss_interrupts_ipc_1_IRQn, /* depends on selected acquireNotifierChannel value */
     #else
         /* .intrSrc            */ cpuss_interrupts_ipc_1_IRQn, /* depends on selected acquireNotifierChannel value */
     #endif
         /* .intrPriority       */ 2u,
     },
     /* .cryptoErrorIntrConfig */ {
     #if (CY_CPU_CORTEX_M0P)
         /* .intrSrc            */ MY_INTR_CRYPTO_ERR_MUX,
         /* .cm0pSrc            */ cpuss_interrupt_crypto_IRQn,
     #else
         /* .intrSrc            */ cpuss_interrupt_crypto_IRQn,
     #endif
         /* .intrPriority       */ 2u,
     }
 };

CY_ALIGN(4) uint8_t  messaggio[64];
CY_ALIGN(4) uint8_t  text[64];
CY_ALIGN(4) uint8_t  chiper[64];

cy_stc_crypto_context_rsa_t     cryptoRsaContext;

int crypto_init()
{
   cryptoStatus = Cy_Crypto_Server_Start_Full(&myCryptoConfig, &myCryptoServerContext);
   if( cryptoStatus != CY_CRYPTO_SUCCESS )
   {
      return 0;
   }   
   cryptoStatus = Cy_Crypto_Init(&myCryptoConfig, &myCryptoContext);
   if( cryptoStatus != CY_CRYPTO_SUCCESS )
   {
      return 0;
   }   
   cryptoStatus = Cy_Crypto_Enable();
   if( cryptoStatus != CY_CRYPTO_SUCCESS )
   {
      return 0;
   }
   
//   
//   messaggio[0] = 'a';
//   messaggio[1] = 't';
//   messaggio[2] = 'a';
//   messaggio[3] = 'l';
//   messaggio[4] = 'a';
//   messaggio[5] = 'n';
//   messaggio[6] = 't';
//   messaggio[7] = 'a';
//   messaggio[8] = 0;
//   messaggio[9] = 0;
//    
//   publicKeyStruct.moduloPtr        = modus;
//   publicKeyStruct.pubExpPtr        = public_exp;
//   publicKeyStruct.moduloLength     = 256;
//   publicKeyStruct.pubExpLength     = 24; 
//   publicKeyStruct.barretCoefPtr    = 0;
//   publicKeyStruct.inverseModuloPtr = 0;
//   publicKeyStruct.rBarPtr          = 0;
//   
//   
//   privateKeyStruct.moduloPtr        = modus;
//   privateKeyStruct.pubExpPtr        = private_exp;
//   privateKeyStruct.moduloLength     = 256;
//   privateKeyStruct.pubExpLength     = 256;
//   privateKeyStruct.barretCoefPtr    = 0;
//   privateKeyStruct.inverseModuloPtr = 0;
//   privateKeyStruct.rBarPtr          = 0;
//
//   //Cy_Crypto_Rsa_InvertEndianness( modus, 256 );
//   
//   cryptoStatus = Cy_Crypto_Rsa_Proc(
//                        &publicKeyStruct, 
//                        (const uint32_t*)messaggio, 
//                        8, 
//                        (uint32_t *)chiper, 
//                        &cryptoRsaContext);
//   
//   cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
//
////   cryptoStatus = Cy_Crypto_Rsa_Proc(
////                        &publicKey.publicKeyStruct,
////                        (const uint32_t*)chiper, sizeof(messaggio), 
////                        (uint32_t *)text, 
////                        &cryptoRsaContext);
////   cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
//   
//   cryptoStatus = Cy_Crypto_Rsa_Proc(
//                        &privateKeyStruct, 
//                        (const uint32_t*)chiper, 
//                        8, 
//                        (uint32_t *)text, 
//                        &cryptoRsaContext);
//
//   cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
   
   return 1;
}
//109662304458830812485176169315834479334961067785135744974881244067947035934931
//82901529641834389038624500130654288268421430229450153569661079869936339159111

int crypto_aes_init()
{
   /* Initialize Crypto AES functionality  */
   cryptoStatus = Cy_Crypto_Aes_Init(
                        (uint32_t *) crypto.aes,
                        CY_CRYPTO_KEY_AES_128,
                        &cryptoAesContext);
    /* ... check for errors... */
    /* Wait crypto become available */
    cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
   
   return 1;
}


void crypto_aes_encrypt( unsigned char* chiper, unsigned char* message, int len )
{
   int block = 0;
   crypto_aes_init();
   do
   {
      /* Encrypt one block (16Bytes) by AES128 */
      cryptoStatus = Cy_Crypto_Aes_Ecb_Run(
                        CY_CRYPTO_ENCRYPT,
                        (uint32_t*)&chiper[block],
                        (uint32_t*)&message[block],
                        &cryptoAesContext);
    
      cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
      if( len > 16 )
      {
         len   = len - 16;
         block = block + 16;
      }
      else len = 0;      
   }while (len > 0);
}


void crypto_aes_decrypt( unsigned char* chiper, unsigned char* message, int len )
{
   int block = 0;
   crypto_aes_init();
   do
   {
      /* Encrypt one block (16Bytes) by AES128 */
      cryptoStatus = Cy_Crypto_Aes_Ecb_Run(
                        CY_CRYPTO_DECRYPT,
                        (uint32_t*)&message[block],
                        (uint32_t*)&chiper[block],
                        &cryptoAesContext);
    
      cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
      if( len > 16 )
      {
         len   = len - 16;
         block = block + 16;
      }
      else len = 0;      
   }while (len > 0);
}

uint8_t tdeskey[24];
void crypto_tdes_encript( unsigned char* chiper, unsigned char* message, int len )
{

   cy_stc_crypto_context_des_t cryptoDesContext;
   cy_en_crypto_status_t       cryptoStatus;

   cryptoStatus = Cy_Crypto_Tdes_Run ( CY_CRYPTO_ENCRYPT,
                                      (uint32_t *)tdeskey,  /* Pointer to keys */
                                      (uint32_t *)chiper,   /* Destination string */
                                      (uint32_t *)message,  /* Source String */
                                      &cryptoDesContext );

   /* ... check for errors... */

   /* Wait crypto become available */
   cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);

   /* ... check for errors... */

}

void crypto_tdes_decript( unsigned char* chiper, unsigned char* message, int len )
{
   cy_stc_crypto_context_des_t cryptoDesContext;
   cy_en_crypto_status_t       cryptoStatus;

   cryptoStatus = Cy_Crypto_Tdes_Run ( CY_CRYPTO_DECRYPT,
                                      (uint32_t *)tdeskey,  /* Pointer to keys */
                                      (uint32_t *)message,  /* Destination string */
                                      (uint32_t *)chiper,   /* Source String */
                                      &cryptoDesContext );

    /* ... check for errors... */

    /* Wait crypto become available */
    cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);

    /* ... check for errors... */

}

void crypto_des_encript( unsigned char* chiper, unsigned char* message, int len )
{
   cy_stc_crypto_context_des_t cryptoDesContext;
   cy_en_crypto_status_t       cryptoStatus;

   cryptoStatus = Cy_Crypto_Des_Run ( CY_CRYPTO_ENCRYPT,
                                      (uint32_t *)crypto.des,  /* Pointer to keys */
                                      (uint32_t *)chiper,   /* Destination string */
                                      (uint32_t *)message,  /* Source String */
                                      &cryptoDesContext );

   /* ... check for errors... */

   /* Wait crypto become available */
   cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);

   /* ... check for errors... */

}

void crypto_des_decript( unsigned char* chiper, unsigned char* message, int len )
{
   cy_stc_crypto_context_des_t cryptoDesContext;
   cy_en_crypto_status_t       cryptoStatus;

   cryptoStatus = Cy_Crypto_Des_Run ( CY_CRYPTO_DECRYPT,
                                      (uint32_t *)crypto.des,    /* Pointer to keys */
                                      (uint32_t *)message,       /* Destination string */
                                      (uint32_t *)chiper,        /* Source String */
                                      &cryptoDesContext );

    /* ... check for errors... */

    /* Wait crypto become available */
    cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);

    /* ... check for errors... */

}



#define CRYPTO_TRNG_GARO_POL             (0x42000000)
#define CRYPTO_TRNG_FIRO_POL             (0x43000000)
#define GARO31_INITSTATE      (0x04c11db7)
#define FIRO31_INITSTATE      (0x04c11db7)

/* Macro for the maximum value of the random number generated in bits */
#define MAX_TRND_VAL                     (8u)

int GenerateRandomKey(int32_t size, uint8_t* buffer)
{
   cy_en_crypto_status_t         cryptoStatus;
   cy_stc_crypto_context_trng_t  TRNGContext;
   
    int32_t  index;
    uint32_t temp;
    for(index = 0; index < size; index++)
    {
        /* Generate a random number */
        cryptoStatus = Cy_Crypto_Trng_Generate( GARO31_INITSTATE, 
                                                FIRO31_INITSTATE, 
                                                MAX_TRND_VAL,
                                                &temp,
                                                &TRNGContext);
           
        if( cryptoStatus != CY_CRYPTO_SUCCESS ) return (int)cryptoStatus;
      
        /* Wait until crypto completes operation */
        cryptoStatus = Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
      
        if( cryptoStatus != CY_CRYPTO_SUCCESS ) return (int)cryptoStatus;
        
        buffer[index] = (uint8_t)temp;
    }
   
   return CY_CRYPTO_SUCCESS;

}
uint32_t GenerateLongRandomNum(uint32_t min, uint32_t max)
{
   uint32_t num = 0;   
   cy_en_crypto_status_t         cryptoStatus;
   cy_stc_crypto_context_trng_t  TRNGContext;

   do
   {
      /* Generate a random number */
      Cy_Crypto_Trng_Generate(CRYPTO_TRNG_GARO_POL, CRYPTO_TRNG_FIRO_POL, 32 , &num , &TRNGContext);

      /* Wait until crypto completes operation */
      Cy_Crypto_Sync(CY_CRYPTO_SYNC_BLOCKING);
   } while( num >= min && num <= max );
   
   return num;
}

void generate_new_key( sCryptoKey* key)
{
   GenerateRandomKey( 16, key->aes );
   GenerateRandomKey(  8, key->des );

   key->opt = 0xaa;
   key->crc = CRC8( (unsigned char*)key, sizeof(sCryptoKey) -1 );
}


// CRC-8 - algoritmo basato sulle formule di CRC-8 di Dallas/Maxim
// codice pubblicato sotto licenza GNU GPL 3.0
unsigned char CRC8( unsigned char *data, unsigned char len) 
{
   unsigned char crc = 0x00;
   unsigned char tempI;
   unsigned char extract;
   unsigned char sum;

   while (len--) {
      extract = *data++;
      for (tempI = 8; tempI; tempI--) {
         sum = (crc ^ extract) & 0x01;
         crc >>= 1;
         if (sum) {
            crc ^= 0x8C;
         }
         extract >>= 1;
      }
   }
   return crc;
}
