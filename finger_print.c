/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "finger_print.h"
#include "project.h"
#include "app.h"

void store_checksum( int len );
void send_combuf( int len );
int read_com(int timeout);
int proc_rxcom();

typedef enum
{
    exe_fp_init = 0,
    exe_fp_attesa,
    exe_fp_run,
    exe_fp_lettura_impronta,
    
    exe_fp_registra_impronta_a,
    exe_fp_registra_impronta_b,
    exe_fp_registra_impronta_c,
    
    exe_fp_elimina_tutte_impronte,
    
    exe_fp_errr
    
    
}exe_finger_print_e;

#define MAX_BUFF 200
char com_buftx[20];
char com_bufrx[MAX_BUFF];
int  com_lenrx;
char impronte[100];

int confirmation_code = -1;

uint8_t impronta_letta;
uint8_t impronta_da_salvare;

exe_finger_print_e fase_fp = exe_fp_init; 

uint16_t timeout_com;

void fingerPrint_task()
{
    int f;
    int t;
    
    switch( fase_fp )
    {
        case exe_fp_init:
        
            SerialeFP_Start();            
            // leggo il fingerprint per vedere quante impronte sono memorizzate
            for( f = 10; f < 100 ; f++ )
            {
                // possono essere memorizzate fino a 90 impronte ( dalla posizione 10 alla posizione 100 )
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 3;   // len
                com_buftx[7] = check_templet; // command
                com_buftx[8] = 0; // command
                com_buftx[9] = f; // command
                
                send_combuf( 10 );
                t = read_com(1000);// == return_ok ? 1 : 0;
                
                if( t == -2 ) f = 100;
                else if( t == return_ok ) impronte[f] = 1;
                else                      impronte[f] = 0;
            }
            fase_fp = exe_fp_attesa;        
        break;
                
        case exe_fp_attesa:
            if( flag.associa_fingerprint )
            {
                fase_fp = exe_fp_registra_impronta_a;
                flag.dito_appoggiato = 0;
            }
            if( flag.dito_appoggiato  )
            {
                
                fase_fp = exe_fp_lettura_impronta;
                flag.dito_appoggiato = 0;
            }
            if( flag.elimina_tutte_impronte )
            {
                fase_fp = exe_fp_elimina_tutte_impronte;
                flag.elimina_tutte_impronte = 0;
            }
        break;
        
        case exe_fp_run:
        
        break;
            
        case exe_fp_lettura_impronta:
            CyDelay(800);
            
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = detecth_finger; // command
                
                send_combuf( 8 );
                if( read_com(1000) != return_ok ) 
                {
                    fase_fp = exe_fp_errr;
                    break;
                }

                //set_led_verdi(1);
                CyDelay(50);
                
                // genera template
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 2;   // len
                com_buftx[7] = gen_templet; // command
                com_buftx[8] = buffer_model;   // buffer 
                
                send_combuf( 9 );
                if( read_com(1000) != return_ok ) 
                {
                    fase_fp = exe_fp_errr;
                    break;
                }                
                //set_led_verdi(3);
                CyDelay(50);
                
                // Find in library
                com_buftx[0]  = 1;   // pacchetto id
                com_buftx[1]  = 0;   // address
                com_buftx[2]  = 0;   // address
                com_buftx[3]  = 0;   // address 
                com_buftx[4]  = 0;   // address
                com_buftx[5]  = 0;   // len 
                com_buftx[6]  = 6;   // len
                com_buftx[7]  = search;   // command
                com_buftx[8]  = buffer_model; // buffer 
                com_buftx[9]  = 0;   // Start page
                com_buftx[10] = 10;  // Start page
                com_buftx[11] = 0;   // Page num
                com_buftx[12] = 90;  // Page num
                
                send_combuf( 13 );
                if( read_com(1000) != return_ok ) 
                {
                    fase_fp = exe_fp_errr;
                    break;
                }                
                                
                impronta_letta = com_bufrx[9];
                flag.impronta_letta = 1;
                
                //sendCode = true;
                //set_led_verdi(7);
                fase_fp = exe_fp_attesa;                
            break;
                
            case exe_fp_registra_impronta_a:
                
                //lampeggia_led_verdi = true;
                
                if( !flag.dito_appoggiato ) break;
                CyDelay(800);
                //lampeggia_led_verdi = false;

                // Detect finger
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = detecth_finger; // command 1

                send_combuf(8);
                if( read_com(1000) != return_ok ) { fase_fp = exe_fp_errr; break; }
                
                //set_led_verdi(1);
                
                // Get image
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = get_image; // command 2

                send_combuf(8);
                read_com(1000);
                
                // Gen template buffer A
                com_buftx[0]  = 1;   // pacchetto id
                com_buftx[1]  = 0;   // address
                com_buftx[2]  = 0;   // address
                com_buftx[3]  = 0;   // address 
                com_buftx[4]  = 0;   // address
                com_buftx[5]  = 0;   // len 
                com_buftx[6]  = 2;   // len
                com_buftx[7]  = gen_templet; // command 3
                com_buftx[8]  = buffer_a;   // buffer A

                send_combuf(9);
                read_com(1000);            
                // prima impronta memorizzata, dovrei accendere un led per conferma
                
                //Seriale_Transmit( "#51\r" , 4 );    // Led verde sulla tastiera per un secondo + Beep
                //invia_a_tastiera_esterna( 5, 1 , 1 );
                set_tastiera_esterna( 'V' , 1, par.en_buzzer_ext, 10 , 1 );
                CyDelay( 1000 );
                set_tastiera_esterna( 3 , 1, par.en_buzzer_ext, 0, 0 );
                //invia_a_tastiera_esterna( 3, 1 , par.en_buzzer_ext ,0,0);
                
                //Seriale_Transmit( "#31\r" , 4 );    // Led verde lampeggiante sulla tastiera
                
                fase_fp = exe_fp_registra_impronta_b;
                // Adesso vedo di prendere una seconda impronta                                
                flag.dito_appoggiato = 0;

            break;
                
            case exe_fp_registra_impronta_b:
                
                if( !flag.dito_appoggiato ) break;
                CyDelay(800);
                // Detect finger
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = detecth_finger; // command 1

                send_combuf(8);
                if( read_com(1000) != return_ok ) { fase_fp = exe_fp_errr; break; }
                
                //set_led_verdi(3);

                // Get image
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = get_image; // command 2
                
                send_combuf(8);
                read_com(1000);
                
                // Gen template buffer B
                com_buftx[0]  = 1;   // pacchetto id
                com_buftx[1]  = 0;   // address
                com_buftx[2]  = 0;   // address
                com_buftx[3]  = 0;   // address
                com_buftx[4]  = 0;   // address
                com_buftx[5]  = 0;   // len
                com_buftx[6]  = 2;   // len
                com_buftx[7]  = gen_templet; // command 3
                com_buftx[8]  = buffer_b;    // buffer B
                
                send_combuf(9);
                read_com(1000);
                
                // Seconda impronta memorizzata, dovrei accendere un led per conferma
                
                //Seriale_Transmit( "#51\r" , 4 );    // Led verde sulla tastiera per un secondo + Beep
                //invia_a_tastiera_esterna( 5, 1 , 1 );
                set_tastiera_esterna( 'V' , 1, par.en_buzzer_ext, 10 , 1 );
                CyDelay( 1000 );
                //invia_a_tastiera_esterna( 3, 1 , par.en_buzzer_ext );
                set_tastiera_esterna( 3 , 1, par.en_buzzer_ext, 0 , 0 );
                //Seriale_Transmit( "#31\r" , 4 );    // Led verde lampeggiante sulla tastiera
                
                fase_fp = exe_fp_registra_impronta_c;
                // Adesso vedo di prendere una terza impronta                                
                flag.dito_appoggiato = 0;
                break;
                
            case exe_fp_registra_impronta_c:
                
                if( !flag.dito_appoggiato ) break;
                CyDelay(800);
                // Detect finger
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = detecth_finger; // command 1
                
                send_combuf(8);
                if( read_com(1000) != return_ok ) { fase_fp = exe_fp_errr; break; }
                
                //set_led_verdi(7);

                // Get image
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = get_image; // command 2

                send_combuf(8);
                read_com(1000);
                
                // Gen template buffer C
                com_buftx[0]  = 1;   // pacchetto id
                com_buftx[1]  = 0;   // address
                com_buftx[2]  = 0;   // address
                com_buftx[3]  = 0;   // address
                com_buftx[4]  = 0;   // address
                com_buftx[5]  = 0;   // len
                com_buftx[6]  = 2;   // len
                com_buftx[7]  = gen_templet; // command 3
                com_buftx[8]  = buffer_c;    // buffer C
                
                send_combuf(9);
                read_com(1000);                
                // Terza impronta memorizzata, dovrei accendere un led per conferma
                
                // Comparo il buffer A e il Buffer B
                com_buftx[0] = 1;   // pacchetto id
                com_buftx[1] = 0;   // address
                com_buftx[2] = 0;   // address
                com_buftx[3] = 0;   // address 
                com_buftx[4] = 0;   // address
                com_buftx[5] = 0;   // len 
                com_buftx[6] = 1;   // len
                com_buftx[7] = merge_two_templet; // command 6

                CyDelay(1000);
                
                send_combuf(8);
                if( read_com(1000) != 0 ) { fase_fp = exe_fp_errr; break; }
                
                // Se ok, lo memorizzo nella sua flash
                com_buftx[0]  = 1;   // pacchetto id
                com_buftx[1]  = 0;   // address
                com_buftx[2]  = 0;   // address
                com_buftx[3]  = 0;   // address 
                com_buftx[4]  = 0;   // address
                com_buftx[5]  = 0;   // len 
                com_buftx[6]  = 4;   // len
                com_buftx[7]  = store_templet; // command 7
                com_buftx[8]  = 3;   // Buffer Model
                com_buftx[9]  = 0;   // Page ID
                com_buftx[10] = impronta_da_salvare;  // Page ID
                
                send_combuf(11);
                read_com(1000);
                
                //Seriale_Transmit( "#61\r" , 4 );    // Led verde sulla tastiera per un secondo + Beep
                //invia_a_tastiera_esterna( 6, 1 , 1 );
                set_tastiera_esterna( 'V' , 1, par.en_buzzer_ext, 10 , 1 );
                // Salvo l'impronta di questo utente
                
                flag.associa_fingerprint = 0;
                flag.dito_appoggiato     = 0;
                fase_fp = exe_fp_attesa;
                break;
            
            case exe_fp_elimina_tutte_impronte:
                
                com_buftx[0]  = 1;   // pacchetto id
                com_buftx[1]  = 0;   // address
                com_buftx[2]  = 0;   // address
                com_buftx[3]  = 0;   // address 
                com_buftx[4]  = 0;   // address
                com_buftx[5]  = 0;   // len 
                com_buftx[6]  = 1;   // len
                com_buftx[7]  = erase_all_templett; // command e

                send_combuf(8);
                if( read_com(1000) != 0 ) { fase_fp = exe_fp_errr; break; }

                /*
                if( par.buzzer_en ) pwm_buzzer_Start();
                
                set_led_rossi(0);
                set_led_verdi(7);
                CyDelay(1000);
                set_led_verdi(0);
                CyDelay(100);
                set_led_verdi(7);
                CyDelay(1000);
                set_led_verdi(0);
                if( par.buzzer_en ) pwm_buzzer_TriggerKill();
*/
                fase_fp = exe_fp_attesa;
            break;
                
               
            case exe_fp_errr:
                
                //Seriale_Transmit( "#21\r" , 4 );    // Led verde sulla tastiera per un secondo + Beep
                //invia_a_tastiera_esterna( 2, 1 , par.en_buzzer_ext );
                set_tastiera_esterna( 'R' , 1, par.en_buzzer_ext, 10 , 1 );
                CyDelay( 1000 );
                
                fase_fp = exe_fp_attesa;
                
                /*
                set_led_verdi(0);
                set_led_rossi(7);
                CyDelay(500);
                set_led_rossi(0);
                CyDelay(500);
                set_led_rossi(7);
                CyDelay(500);
                set_led_rossi(0);
                
                ++logUfg.errori;

                if(  memorizzoImpronta == true )
                {
                    if( ++tentativi_memorizzazzione_impronta == 3 )
                    {
                        memorizzoImpronta   = false;
                        impronta_da_inviare = 0;
                        sendCode = true;
                        fase_fp  = invia;
                        break; 
                    }
                    fl_impronta_letta = 0;
                    fase_fp = registra_impronta_a;
                    break;
                }
                else
                {
                    fase_fp = sleep;            
                }
                */
            break;            
    }
    
}

void send_combuf( int len )
{
    store_checksum( len );
    
    SerialeFP_Put(0xc0);
    SerialeFP_PutArray( com_buftx, len+2);
    SerialeFP_Put(0xc0);
    
    while( SerialeFP_IsTxComplete() )
    {}
    /*
    com_Put(0xc0);
    com_PutArray( com_buftx, len + 2 );
    com_Put(0xc0);
                        
    while( com_IsTxComplete() )
    {
    }
    */

}

void store_checksum( int len )
{
    int a;
    uint16_t c = 0;
    
    for( a=0; a < len; a++ )
    {
        c += (uint16_t)com_buftx[a];    
    }
    
    com_buftx[len]   = (char)(c >> 8);
    com_buftx[len+1] = (char)(c & 0xff);
}

int read_com(int timeout)
{
    static int fase_rx = 0;
    
    char rx;
    cy_en_scb_uart_status_t status;
    timeout_com = timeout;
    confirmation_code = -1;
    
    memset( com_bufrx,0,50);
    
    while( timeout_com )
    {    
        if(SerialeFP_GetNumInRxFifo() )
        {
            status = SerialeFP_Receive( &rx,1);      
            // La trasmissione inizia e finisce con 0xc0
            // se ricevo 0xdb controllo il dato seguente
            // se il seguente è 0xdc salvo 0xc0
            // se il seguente è 0xdd salvo 0xdb
            switch(fase_rx)
            {
                case 0:
                    com_lenrx = 0;
                    if( rx == 0xc0 ) fase_rx = 1;
                break;
                case 1 :                
                    if( rx == 0xdb ) fase_rx = 2;
                    else if( rx == 0xc0 ) 
                    {
                        confirmation_code = proc_rxcom();
                        fase_rx     = 0;
                        return confirmation_code;
                        //timeout_com = 0;
                    }
                    else if( com_lenrx < MAX_BUFF )
                    {
                        com_bufrx[ com_lenrx++ ] = rx;            
                    }
                    else fase_rx = 0;
                        
                break;
                case 2:
                    if(      rx == 0xdc ) com_bufrx[ com_lenrx++ ] = 0xc0;
                    else if( rx == 0xdd ) com_bufrx[ com_lenrx++ ] = 0xdb;
                    fase_rx = 1;
                break;
            }
        }
    }
    confirmation_code = -2;
    return confirmation_code;
}

int proc_rxcom()
{
    if( com_bufrx[0] == 0x07 )
    {
        // Restituisco il confirmation code
        return com_bufrx[7];
    }
    return -1;
}


/* [] END OF FILE */
