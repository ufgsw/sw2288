/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

// indirizzamento in FRam ( max 65536 byte )
#define ADDRESS_PAR         0x0000    // size = 0x0058
#define ADDRESS_LOG_SER     0x0090    // size = 0x00f2
#define ADDRESS_CLOSER      0x0190    // size = 0x0015
#define ADDRESS_LOGUFG      0x01b0    // size = 0x0014
#define ADDRESS_NEWFW       0x0220    // size = 0x0001
#define ADDRESS_KEY         0x0230    // size = 0x0024
#define ADDRESS_KEY_COPY    0x0260    // size = 0x0024
#define ADDRESS_NUM_UTENTI  0x0300    // size = 0x0002
#define ADDRESS_UTENTI      0x0302    // size = 0xc030     52800
#define ADDRESS_BRIDGEID    0xe000    // size = 0x0014

#define cs_on()   Cy_GPIO_Write( cs_fram_PORT, cs_fram_NUM, 0 );
#define cs_off()  Cy_GPIO_Write( cs_fram_PORT, cs_fram_NUM, 1 );

typedef enum
{
   cmd_wrsr  = 0x01,
   cmd_read  = 0x03,
   cmd_write = 0x02,
   cmd_wrdi  = 0x04,
   cmd_rdsr  = 0x05,
   cmd_wren  = 0x06,
   cmd_rdid  = 0x9f,
   cmd_fstrd = 0x0b,
   cmd_sleep = 0xb9
}eCmd;

typedef struct
{
    unsigned WIP : 1;
    unsigned WEL : 1;
    unsigned BP0 : 1;
    unsigned BP1 : 1;
    unsigned RESERVED : 3;
    unsigned WPEN : 1;
}sFramStatusReg;

typedef union
{
    sFramStatusReg  bits;
    uint8_t         val;
}uFramStatusReg;


bool fram_init();
bool fram_readId();
bool fram_writeByte( uint16_t address, uint8_t  dato );
bool fram_readByte(  uint16_t address, uint8_t* dato );
bool fram_writeArray(uint16_t address, uint8_t* arr  ,int count);
bool fram_readArray( uint16_t address, uint8_t* arr  ,int count);
bool fram_sleep();
bool fram_wakeup();

/* [] END OF FILE */
