/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef ERROR_H
#define ERROR_H
    
typedef enum
{
    err_ok = 0,
    err_credenziali ,
    err_orario ,
    err_permesso_disabilitato ,
    err_permesso_telefono ,
    err_affitto_scaduto ,
    err_verifica_random_code ,
    err_max_utenti ,
    err_codice_esistente ,
    err_link_rfid ,
    err_link_finger ,
    err_pair_device ,
    err_utente_esistente ,
    err_nodata ,
    err_save_data,
    err_operazione_non_permessa,
    err_maschera_apertura
} eErrorCode;    
    
#endif    

/* [] END OF FILE */
