/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
/* Macros for the polynomial to configure the programmable Galois and Fibonacci
   ring oscillators */

#define AES128_KEY_LENGTH         (uint32_t)(16u)
#define MAX_MESSAGE_SIZE          (uint32_t)(128u)
#define AES128_ENCRYPTION_LENGTH  (uint32_t)(16u)
#define CRYPTO_BLOCKING           true

#define MY_CHAN_CRYPTO         (uint32_t)(3u)    /* IPC data channel for the Crypto */
#define MY_INTR_CRYPTO_SRV     (uint32_t)(1u)    /* IPC interrupt structure for the Crypto server */
#define MY_INTR_CRYPTO_CLI     (uint32_t)(2u)    /* IPC interrupt structure for the Crypto client */
#define MY_INTR_CRYPTO_SRV_MUX (IRQn_Type)(2u)   /* CM0+ IPC interrupt mux number the Crypto server */
#define MY_INTR_CRYPTO_CLI_MUX (IRQn_Type)(3u)   /* CM0+ IPC interrupt mux number the Crypto client */
#define MY_INTR_CRYPTO_ERR_MUX (IRQn_Type)(4u)   /* CM0+ ERROR interrupt mux number the Crypto server */

typedef struct
{
   CY_ALIGN(4) uint8_t aes[16];   
   CY_ALIGN(4) uint8_t des[8];   
   
   uint8_t  mastercode[10];   
   uint8_t  opt;
   uint8_t  crc;
}sCryptoKey;
extern sCryptoKey crypto;

//
//#define RSA_MODULO_LENGTH       512u
//#define RSA_MODULO_DATA_SIZE    (RSA_MODULO_LENGTH / 8)
//
//typedef struct
//{
//  cy_stc_crypto_rsa_pub_key_t publicKeyStruct;
//  uint8_t  moduloData[RSA_MODULO_DATA_SIZE];
//  uint8_t  expData   [32];
//  uint8_t  k1Data    [RSA_MODULO_DATA_SIZE+4];
//  uint8_t  k2Data    [RSA_MODULO_DATA_SIZE];
//  uint8_t  k3Data    [RSA_MODULO_DATA_SIZE];
//} cy_stc_public_key_t;
//
//typedef struct
//{
//  cy_stc_crypto_rsa_pub_key_t privateKeyStruct;
//  uint8_t  moduloData[RSA_MODULO_DATA_SIZE];
//  uint8_t  expData   [RSA_MODULO_DATA_SIZE];
//  uint8_t  k1Data    [RSA_MODULO_DATA_SIZE+4];
//  uint8_t  k2Data    [RSA_MODULO_DATA_SIZE];
//  uint8_t  k3Data    [RSA_MODULO_DATA_SIZE];
//} cy_stc_private_key_t;

int  crypto_init();
int  crypto_aes_init();
void crypto_aes_encrypt( unsigned char* chiper, unsigned char* message, int len );
void crypto_aes_decrypt( unsigned char* chiper, unsigned char* message, int len );

void crypto_tdes_encript( unsigned char* chiper, unsigned char* message, int len );
void crypto_tdes_decript( unsigned char* chiper, unsigned char* message, int len );

void crypto_des_encript( unsigned char* chiper, unsigned char* message, int len );
void crypto_des_decript( unsigned char* chiper, unsigned char* message, int len );

void generate_new_key( sCryptoKey* key);

int      GenerateRandomKey(int32_t size, uint8_t* buffer);
uint32_t GenerateLongRandomNum(uint32_t min, uint32_t max);

int64_t gcd(int64_t num1 , int64_t num2);

unsigned char CRC8( unsigned char *data, unsigned char len);
