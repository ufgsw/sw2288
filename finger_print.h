/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdint.h>

typedef enum
{
    detecth_finger     = 0x01,
    get_image          = 0x02,
    gen_templet        = 0x03,
    move_templet       = 0x20,
    match_two_templet  = 0x04,
    search             = 0x05,
    merge_two_templet  = 0x06,
    store_templet      = 0x07,
    load_templet       = 0x08,
    up_templet         = 0x09,
    down_templet       = 0x0a,
    delete_one_templet = 0x0d,
    erase_all_templett = 0x0e,
    read_par_table     = 0x0f,
    set_secure_level   = 0x12,
    set_pwd            = 0x13,
    verify_pwd         = 0x14,
    sys_reset          = 0x15,
    flash_led          = 0x16,
    check_templet      = 0x28,
    check_dw_status    = 0x31,
    set_bps            = 0x33,
    up_image           = 0x38        
}command_e;

typedef enum
{
    buffer_a     = 0x01,
    buffer_b     = 0x02,
    buffer_model = 0x03,
    buffer_c     = 0x04
}buffer_e;

typedef enum
{
    return_ok                                   = 0x00,
    return_packet_error                         = 0x01,
    return_no_finger                            = 0x02,
    return_failed_enter_finger                  = 0x03,
    return_image_is_too_dry                     = 0x04,
    return_image_is_too_wet                     = 0x05,
    return_image_is_too_messy                   = 0x06,
    return_image_ok_points_err                  = 0x07,
    retun_no_valid_image_in_buffer              = 0x15,
    return_fingerprint_mismatch                 = 0x08,
    return_no_fingerprint_found                 = 0x09,
    return_feature_merge_failed                 = 0x0a,
    return_address_serial_err                   = 0x0b,
    return_err_reading_template                 = 0x0c,
    return_upload_feature_failed                = 0x0d,
    return_module_no_accept_packets             = 0x0e,
    return_failed_upload_image                  = 0x0f,
    return_failed_delete_template               = 0x10,
    return_failed_to_empty_fingerprint_library  = 0x11,
    return_unable_to_enter_sleep                = 0x12,
    return_instruction_err                      = 0x13,
    return_reset_failed                         = 0x14,
    return_current_image_no_valid               = 0x15,
    return_operation_flash_err                  = 0x18,
    return_no_valid_template                    = 0x19,
    return_download_instruction_failed          = 0x24,
    return_setting_wave_rate_failed             = 0x33
}confirmation_code_e;


void fingerPrint_task();

extern uint16_t timeout_com;
extern uint8_t  impronta_da_salvare;
extern uint8_t  impronta_letta;

/* [] END OF FILE */
