/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "main_cm4.h"
#include "app.h"
#include "crypto.h"
#include "ble/cy_ble_stack_gatt.h"

//#define GET_LOG 1

CY_SECTION(".cy_app_signature")
__USED const uint32_t cy_bootload_appSignature[1];

#define ADV_TIMER_TIMEOUT (3u)
#define CYBLE_GAPP_CONNECTION_INTERVAL_MIN (0x000Cu) /* 15 ms - (N * 1,25)*/
#define CYBLE_GAPP_CONNECTION_INTERVAL_MAX (0x000Cu) /* 15 ms */
#define CYBLE_GAPP_CONNECTION_SLAVE_LATENCY (0x0000u)
#define CYBLE_GAPP_CONNECTION_TIME_OUT (0x00C8u) /* 2000 ms */

void BLE_CallBack(uint32_t event, void *eventParam);
void US_ScambioDati(cy_stc_ble_conn_handle_t connHandle);

static void StartAdvertisement(void);
static void disconnettiTutti(void);

cy_en_ble_api_result_t apiResult;
cy_stc_ble_timer_info_t timerParam = {.timeout = ADV_TIMER_TIMEOUT};

volatile uint32_t mainTimer = 1u;
cy_en_sysclk_status_t status_clk;
cy_en_ble_adv_state_t status_adv;

cy_stc_ble_gatt_err_info_t gatt_err_info;

int r;
bool ble_stack_ok = false;
uint16_t connIntv;

int ble_connessi = 0;

bool main_start = false;


int main(void)
{
    revision = Cy_SysLib_GetDeviceRevision();
    error.bit.wco = !Cy_SysClk_WcoOkay();

//    ANA_Start();
    RTC_Start();
    wdog_Start();
    
    Seriale_Start();

    Cy_SysInt_Init(&SysInt_tasto_cfg, isrTasto);

    NVIC_ClearPendingIRQ(SysInt_tasto_cfg.intrSrc);
    NVIC_EnableIRQ(SysInt_tasto_cfg.intrSrc);

    //Cy_SysInt_Init(&SysInt_port10_cfg, isrCitofono);
    //NVIC_ClearPendingIRQ(SysInt_port10_cfg.intrSrc);
    //NVIC_EnableIRQ(SysInt_port10_cfg.intrSrc);

    Cy_SysInt_Init(&SysInt_port9_cfg, isrCapsense);
    NVIC_ClearPendingIRQ(SysInt_port9_cfg.intrSrc);
    Cy_GPIO_ClearInterrupt(touch_fingerprint_PORT, touch_fingerprint_NUM );
    Cy_GPIO_ClearInterrupt(pin_wakeup_ext_PORT, pin_wakeup_ext_0_NUM);
    NVIC_EnableIRQ(SysInt_port9_cfg.intrSrc);

    Cy_SysInt_Init(&RTC_RTC_IRQ_cfg, &isr_RTC);
    NVIC_EnableIRQ(RTC_RTC_IRQ_cfg.intrSrc);

    Cy_SysTick_Init(CY_SYSTICK_CLOCK_SOURCE_CLK_CPU, 48000);
    Cy_SysTick_SetCallback(0, tick_1ms);
    Cy_SysTick_Enable();

    __enable_irq(); /* Enable global interrupts. */

    /* Start BLE component and register generic event handler */
    apiResult = Cy_BLE_Start(BLE_CallBack);
    if (apiResult != CY_BLE_SUCCESS)
    {
    }

    CyDelay(1000);

    apiResult = Cy_BLE_BASS_SetCharacteristicValue(CY_BLE_BAS_BATTERY_LEVEL, CY_BLE_BAS_BATTERY_LEVEL, 1, &batteria);

    //   Cy_LVD_ClearInterruptMask();
    //   Cy_LVD_SetThreshold(CY_LVD_THRESHOLD_2_0_V);
    //   Cy_LVD_SetInterruptConfig(CY_LVD_INTR_BOTH);
    //   Cy_LVD_Enable();
    //   Cy_SysLib_DelayUs(20U);
    //   Cy_LVD_ClearInterrupt();
    //   Cy_LVD_SetInterruptMask();

    for( r = 0; r < 4 ; r++ )
    {
        gapDevice[ r ].connHandle.attId    = 0;
        gapDevice[ r ].connHandle.bdHandle = CY_BLE_INVALID_CONN_HANDLE_VALUE;
        gapDevice[ r ].faseSend   = 0;
        gapDevice[ r ].errCode    = 0;
        gapDevice[ r ].errCommand = 0;        
    }

    
    main_start = true;
    for (;;)
    {
        /* Place your application code here. */
        Cy_BLE_ProcessEvents();
        
        //Cy_BLE_GetTemperature();
        // ---------------------------------------------------------------
        wdog_ResetCounters(CY_MCWDT_CTR0 | CY_MCWDT_CTR1, 62 );
        // ---------------------------------------------------------------

        //   CY_BLE_STATE_STOPPED       | BLE is turned off.
        //   CY_BLE_STATE_INITIALIZING  | BLE is initializing (waiting for #CY_BLE_EVT_STACK_ON event).
        //   CY_BLE_STATE_ON            | BLE is turned on.
        //   CY_BLE_STATE_CONNECTING

        if (Cy_BLE_GetState() == CY_BLE_STATE_STOPPED)
        {
            Cy_BLE_Start(BLE_CallBack);       
        }
        
        if(  Cy_BLE_GetNumOfActiveConn() > 0 ) flag.ble_connect = 1;
        else                                   flag.ble_connect = 0;
        
        if( flag.ble_connect )
        {
            BLE_Data();
            if (timeout_ble == 0)
            {
                disconnettiTutti();
            }
            if( timeout_mastercode == 0 )
            {
                gapDevice[0].master_ok = 0;
                gapDevice[1].master_ok = 0;
                gapDevice[2].master_ok = 0;
                gapDevice[3].master_ok = 0;
            }
        }
        else 
        {
            flag.notifica = 0;
            timeout_ble   = 0;
            timeout_mastercode = 0;
                        
            gapDevice[0].faseSend = send_null;
            gapDevice[1].faseSend = send_null;
            gapDevice[2].faseSend = send_null;
            gapDevice[3].faseSend = send_null;
            
            gapDevice[0].master_ok = 0;
            gapDevice[1].master_ok = 0;
            gapDevice[2].master_ok = 0;
            gapDevice[3].master_ok = 0;
        }

        // ---------------------------------------------------------------
        // Advertising
        // ---------------------------------------------------------------

        if (ble_stack_ok == true )
        {
            status_adv = Cy_BLE_GetAdvertisementState();
            if (status_adv == CY_BLE_ADV_STATE_ADVERTISING)
            {
                if (flag.ble_update_adv)
                {
                    // Una volta fermato l'advertising ripartirÃ  in SLOW con i dati aggiornati ( CY_BLE_EVT_GAPP_ADVERTISEMENT_START_STOP )
                    flag.ble_update_adv = 0;
                    Cy_BLE_GAPP_StopAdvertisement();
                    // Attendo che si Ã¨ fermato
                    while (Cy_BLE_GetAdvertisementState() != CY_BLE_ADV_STATE_STOPPED)
                    {
                        Cy_BLE_ProcessEvents();
                    }
                    // Riparto
                    StartAdvertisement();
                }
            }
            else if (status_adv == CY_BLE_ADV_STATE_STOPPED)
            {
                StartAdvertisement();
            }
        }

        /* Restart 1s timer */
        if (mainTimer != 0u)
        {
            mainTimer = 0u;
            Cy_BLE_StartTimer(&timerParam);
        }

        // Se rischiesto un nuovo firmware riparto dal bootloader
        if (flag.start_boot)
        {
            disconnettiTutti();
            /* Stop BLE component. */
            Cy_BLE_Disable();
            Cy_DFU_ExecuteApp(0u);
        }
        //
        applicationTask();
    }
}

static void StartAdvertisement(void)
{
    unsigned int cloud_event;
    /* Variable used to store the return values of BLE APIs */
    cy_en_ble_api_result_t bleApiResult;
    
    /* Get the number of active connection */
    uint8_t numActiveConn = Cy_BLE_GetNumOfActiveConn();    
           
    // Start Advertisement and enter discoverable mode. Make sure that BLE is neither connected nor advertising already 
    
    if((Cy_BLE_GetAdvertisementState() == CY_BLE_ADV_STATE_STOPPED) && (numActiveConn < CY_BLE_CONN_COUNT) )
    {
        if (par.tipo_alimentazione == alimentazione_rete)
            cy_ble_discoveryData[0].advData[25] = 200;
        else
            cy_ble_discoveryData[0].advData[25] = batteria;

        cy_ble_discoveryData[0].advData[26] = statoIngressi.val;

        cloud_event = ( flag.send_event_tocloud | flag.send_user_tocloud | flag.send_par_tocloud | flag.send_users_tocloud | flag.send_wifi_tobridge | flag.send_all_tocloud );
            
        adv.type.nuova           = par.nuova & 0x01;
        adv.type.abbina_tastiera = flag.ble_accoppia_tastiera;
        adv.type.abbina_telefono = flag.ble_accoppia_telefono;
        adv.type.abbina_impronta = 0;
        adv.type.recovery        = flag.recovery;
        adv.type.abbina_bridge   = flag.ble_accoppia_bridge;
        adv.type.disabilitata    = flag.serratura_disabilitata; 

        adv.type.nuovo_evento    = cloud_event ? bridge.accoppiato == 1 : 0;
            
        cy_ble_discoveryData[0].advData[27] = adv.val;
        cy_ble_discoveryData[0].advData[28] = tipo_scheda;
        cy_ble_discoveryData[0].advData[29] = RELEASE_PROTO;
        cy_ble_discoveryData[0].advData[30] = 0;

        apiResult = Cy_BLE_BASS_SetCharacteristicValue(CY_BLE_BAS_BATTERY_LEVEL, CY_BLE_BAS_BATTERY_LEVEL, 1, &batteria);

        if (par.tipo_alimentazione == alimentazione_rete)
            apiResult = Cy_BLE_GAPP_StartAdvertisement(CY_BLE_ADVERTISING_FAST, CY_BLE_PERIPHERAL_CONFIGURATION_0_INDEX);
        else
            apiResult = Cy_BLE_GAPP_StartAdvertisement(CY_BLE_ADVERTISING_SLOW, CY_BLE_PERIPHERAL_CONFIGURATION_0_INDEX);

        if (apiResult != CY_BLE_SUCCESS)
        {
            // Che faccio ?
        }        
    }
    
    /* If no device is connected then start the 30 sec timer */
    if(numActiveConn == 0)
    {
        /* Start the timer */
        //xTimerReset(xBleTimer, 0u);
        //xTimerStart(xBleTimer, 0u);
    }
}

static void disconnettiTutti(void)
{
    unsigned int a;    
    cy_stc_ble_gap_disconnect_info_t disconnectInfoParam;
    
    for( a = 0; a < CY_BLE_MAX_CONNECTION_INSTANCES; a++ )
    {
        disconnectInfoParam.bdHandle = gapDevice[a].connHandle.bdHandle,
        disconnectInfoParam.reason = CY_BLE_HCI_ERROR_OTHER_END_TERMINATED_USER;

        /* Initiate disconnection from the peer device*/
        if (Cy_BLE_GAP_Disconnect(&disconnectInfoParam) == CY_BLE_SUCCESS)
        {
            /* Wait for disconnection event */
            while (Cy_BLE_GetConnectionState( gapDevice[a].connHandle ) == CY_BLE_CONN_STATE_CONNECTED)
            {
                /* Process BLE events */
                Cy_BLE_ProcessEvents();
            }
        }
    }
}

uint8_t connectedBdHandle;
void BLE_CallBack(uint32_t event, void *eventParam)
{
    cy_stc_ble_gatts_write_cmd_req_param_t *wrReqParam; // = (cy_stc_ble_gatts_write_cmd_req_param_t *)eventParam;
    //cy_stc_ble_gatts_char_val_read_req_t*   rdReqParam;
    cy_en_ble_api_result_t apiResult;
    cy_en_ble_gatt_err_code_t gattErr;
    cy_stc_ble_gatt_err_info_t errInfo;
    cy_stc_ble_gatts_db_attr_val_info_t gattsDbInfo;
    cy_stc_ble_gatt_handle_value_pair_t gattHandleValuePair;
    
    static cy_stc_ble_conn_handle_t connHandle;

    //static uint8_t *enable_notification_sensori;

    switch (event)
    {
    case CY_BLE_EVT_INVALID:
        Cy_BLE_Stop();
        break;
    case CY_BLE_EVT_STACK_ON:
        /* Enter into discoverable mode so that remote can find it. */
        Cy_BLE_SetLocalName((const char *)par.localname);
        //         cy_ble_discoveryData[0].advData[25] = batteria;
        //         cy_ble_discoveryData[0].advData[26] = statoPorta.val;
        //         cy_ble_discoveryData[0].advData[27] = par.nuova;
        //         apiResult = Cy_BLE_GAPP_StartAdvertisement(CY_BLE_ADVERTISING_FAST, CY_BLE_PERIPHERAL_CONFIGURATION_0_INDEX);
        //         if(apiResult != CY_BLE_SUCCESS)
        //         {
        //         }
#ifdef GET_LOG
        Seriale_PutString("STACK BLE ON");
#endif    
        
        ble_stack_ok = true;
        StartAdvertisement();
        
        break;

    case CY_BLE_EVT_TIMEOUT: /* 0x01 -> GAP limited discoverable mode timeout. */
                             /* 0x02 -> GAP pairing process timeout. */
                             /* 0x03 -> GATT response timeout. */
        if ((((cy_stc_ble_timeout_param_t *)eventParam)->reasonCode == CY_BLE_GENERIC_APP_TO) &&
            (((cy_stc_ble_timeout_param_t *)eventParam)->timerHandle == timerParam.timerHandle))
        {
            /* Indicate that timer is raised to the main loop */
            mainTimer++;
        }
        else
        {
            //DBG_PRINTF("CY_BLE_EVT_TIMEOUT: %d \r\n", *(uint8_t *)eventParam);
        }
        break;

    case CY_BLE_EVT_HARDWARE_ERROR:
        /* This event indicates that some internal HW error has occurred. */
        error.bit.ble_hw = 1;
        break;

    /* This event will be triggered by host stack if BLE stack is busy or not busy.
         *  Parameter corresponding to this event will be the state of BLE stack.
         *  BLE stack busy = CY_BLE_STACK_STATE_BUSY,
         *  BLE stack not busy = CY_BLE_STACK_STATE_FREE 
         */
    case CY_BLE_EVT_STACK_BUSY_STATUS:

        break;

    case CY_BLE_EVT_SET_TX_PWR_COMPLETE:
        break;

    case CY_BLE_EVT_LE_SET_EVENT_MASK_COMPLETE:
        break;

    case CY_BLE_EVT_SET_DEVICE_ADDR_COMPLETE:
        /* Reads the BD device address from BLE Controller's memory */
        apiResult = Cy_BLE_GAP_GetBdAddress();
        if (apiResult != CY_BLE_SUCCESS)
        {
        }
        break;

    case CY_BLE_EVT_GET_DEVICE_ADDR_COMPLETE:
        //DBG_PRINTF("CY_BLE_EVT_GET_DEVICE_ADDR_COMPLETE: ");
        bdAddress[0] = ((cy_stc_ble_bd_addrs_t *)((cy_stc_ble_events_param_generic_t *)eventParam)->eventParams)->publicBdAddr[0];
        bdAddress[1] = ((cy_stc_ble_bd_addrs_t *)((cy_stc_ble_events_param_generic_t *)eventParam)->eventParams)->publicBdAddr[1];
        bdAddress[2] = ((cy_stc_ble_bd_addrs_t *)((cy_stc_ble_events_param_generic_t *)eventParam)->eventParams)->publicBdAddr[2];
        bdAddress[3] = ((cy_stc_ble_bd_addrs_t *)((cy_stc_ble_events_param_generic_t *)eventParam)->eventParams)->publicBdAddr[3];
        bdAddress[4] = ((cy_stc_ble_bd_addrs_t *)((cy_stc_ble_events_param_generic_t *)eventParam)->eventParams)->publicBdAddr[4];
        bdAddress[5] = ((cy_stc_ble_bd_addrs_t *)((cy_stc_ble_events_param_generic_t *)eventParam)->eventParams)->publicBdAddr[5];
        break;

    case CY_BLE_EVT_STACK_SHUTDOWN_COMPLETE:
        /* Hibernate */
        //Cy_SysPm_Hibernate();
        break;

        /**********************************************************
        *                       GAP Events
        ***********************************************************/

    case CY_BLE_EVT_GAP_AUTH_REQ:
        /* This event is received by Peripheral and Central devices. When it is received by a peripheral, 
             * that peripheral must Call Cy_BLE_GAPP_AuthReqReply() to reply to the authentication request
             * from Central. */

        //            DBG_PRINTF("CY_BLE_EVT_GAP_AUTH_REQ: bdHandle=%x, security=%x, bonding=%x, ekeySize=%x, err=%x \r\n",
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).bdHandle, (*(cy_stc_ble_gap_auth_info_t *)eventParam).security,
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).bonding, (*(cy_stc_ble_gap_auth_info_t *)eventParam).ekeySize,
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).authErr);

        if (cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].security == (CY_BLE_GAP_SEC_MODE_1 | CY_BLE_GAP_SEC_LEVEL_1))
        {
            cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].authErr = CY_BLE_GAP_AUTH_ERROR_PAIRING_NOT_SUPPORTED;
        }

        cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].bdHandle = ((cy_stc_ble_gap_auth_info_t *)eventParam)->bdHandle;

        /* Pass security information for authentication in reply to an authentication request 
             * from the master device */
        apiResult = Cy_BLE_GAPP_AuthReqReply(&cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX]);
        if (apiResult != CY_BLE_SUCCESS)
        {
            Cy_BLE_GAP_RemoveOldestDeviceFromBondedList();
            apiResult = Cy_BLE_GAPP_AuthReqReply(&cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX]);
            if (apiResult != CY_BLE_SUCCESS)
            {
                //DBG_PRINTF("Cy_BLE_GAPP_AuthReqReply API Error: 0x%x \r\n", apiResult);
            }
        }
        break;

    case CY_BLE_EVT_GAP_SMP_NEGOTIATED_AUTH_INFO:
        //            DBG_PRINTF("CY_BLE_EVT_GAP_SMP_NEGOTIATED_AUTH_INFO:"
        //                       " bdHandle=%x, security=%x, bonding=%x, ekeySize=%x, err=%x \r\n",
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).bdHandle,
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).security,
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).bonding,
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).ekeySize,
        //                (*(cy_stc_ble_gap_auth_info_t *)eventParam).authErr);
        break;

    case CY_BLE_EVT_GAP_PASSKEY_ENTRY_REQUEST:
        //            DBG_PRINTF("CY_BLE_EVT_GAP_PASSKEY_ENTRY_REQUEST\r\n");
        //            DBG_PRINTF("Please enter the passkey displayed on the peer device:\r\n");
        //            App_SetAuthIoCap(CY_BLE_GAP_IOCAP_KEYBOARD_ONLY);
        break;

    case CY_BLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST:
        //            DBG_PRINTF("CY_BLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST: %6.6ld\r\n", *(uint32_t *)eventParam);
        //            App_SetAuthIoCap(CY_BLE_GAP_IOCAP_DISPLAY_ONLY);
        break;

    case CY_BLE_EVT_GAP_NUMERIC_COMPARISON_REQUEST:
        //            DBG_PRINTF("Compare this passkey with the one displayed in your peer device and press 'y' or 'n':"
        //                       " %6.6lu \r\n", *(uint32_t *)eventParam);
        //            App_SetAuthIoCap(CY_BLE_GAP_IOCAP_DISPLAY_YESNO);
        break;

    case CY_BLE_EVT_GAP_AUTH_FAILED:
        //            DBG_PRINTF("CY_BLE_EVT_GAP_AUTH_FAILED, reason: ");
        //            App_ShowAuthError(((cy_stc_ble_gap_auth_info_t *)eventParam)->authErr);
        break;
#if (CY_BLE_LL_PRIVACY_FEATURE_ENABLED)
    case CY_BLE_EVT_GAP_ENHANCE_CONN_COMPLETE:
    {
        cy_stc_ble_gap_enhance_conn_complete_param_t *connParameters = (cy_stc_ble_gap_enhance_conn_complete_param_t *)eventParam;
        timeout_ble = 10000;
        flag.ble_connect = 1;
#else
    case CY_BLE_EVT_GAP_DEVICE_CONNECTED:
    {
        int a;
        cy_stc_ble_gap_connected_param_t *connParameters = (cy_stc_ble_gap_connected_param_t *)eventParam;
        
//        connParameters->bdHandle
//        
//        if (((*(cy_stc_ble_gap_connected_param_t *)eventParam).connIntv < CYBLE_GAPP_CONNECTION_INTERVAL_MIN) || ((*(cy_stc_ble_gap_connected_param_t *)eventParam).connIntv > CYBLE_GAPP_CONNECTION_INTERVAL_MAX))
//        {
//            cy_stc_ble_gap_conn_update_param_info_t connUpdateParam;
//            /* If connection settings do not match expected ones - request parameter update */
//            connUpdateParam.connIntvMin = CYBLE_GAPP_CONNECTION_INTERVAL_MIN;
//            connUpdateParam.connIntvMax = CYBLE_GAPP_CONNECTION_INTERVAL_MAX;
//            connUpdateParam.connLatency = CYBLE_GAPP_CONNECTION_SLAVE_LATENCY;
//            connUpdateParam.supervisionTO = CYBLE_GAPP_CONNECTION_TIME_OUT;
//            connUpdateParam.bdHandle = appConnHandle.bdHandle;
//            apiResult = Cy_BLE_L2CAP_LeConnectionParamUpdateRequest(&connUpdateParam);
//            //DBG_PRINTF("Cy_BLE_L2CAP_LeConnectionParamUpdateRequest API: 0x%2.2x \r\n", apiResult);
//        }

        for( a=0;a<4;a++)
        {
            if( gapDevice[a].connHandle.bdHandle == connParameters->bdHandle )
            {
                gapDevice[a].bdAddress[0] = connParameters->peerAddr[0];    
                gapDevice[a].bdAddress[1] = connParameters->peerAddr[1];    
                gapDevice[a].bdAddress[2] = connParameters->peerAddr[2];    
                gapDevice[a].bdAddress[3] = connParameters->peerAddr[3];    
                gapDevice[a].bdAddress[4] = connParameters->peerAddr[4];    
                gapDevice[a].bdAddress[5] = connParameters->peerAddr[5];    
            }
        }
        
        StartAdvertisement(); // start advertising x gli altri
        if( timeout_ble < 20000 )  timeout_ble = 20000;
        
        

        //connIntv = ((cy_stc_ble_gap_connected_param_t *)eventParam)->connIntv;
        //connectedBdHandle = ((cy_stc_ble_gap_connected_param_t *)eventParam)->bdHandle;
    }
#endif
        /* CY_BLE_LL_PRIVACY_FEATURE_ENABLED */
        /* Continue connection case without a break */

        /* Initiate pairing process */
        //            if((cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].security & CY_BLE_GAP_SEC_LEVEL_MASK) >
        //                CY_BLE_GAP_SEC_LEVEL_1)
        //            {
        //                cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].bdHandle = appConnHandle.bdHandle;
        //                apiResult = Cy_BLE_GAP_AuthReq(&cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX]);
        //                if(apiResult != CY_BLE_SUCCESS)
        //                {
        //                    //DBG_PRINTF(" Cy_BLE_GAP_AuthReq API Error: 0x%x \r\n", apiResult);
        //                }
        //            }
        //            else
        //            {
        //                authState = AUTHENTICATION_UPDATE_CONN_PARAM;
        //            }
        //            //UpdateLedState();
        //
        //            /* Set security keys for new device which is not already bonded */
        //            if(IsDeviceInBondList(connectedBdHandle) == 0u)
        //            {
        //                keyInfo.SecKeyParam.bdHandle = connectedBdHandle;
        //                apiResult = Cy_BLE_GAP_SetSecurityKeys(&keyInfo);
        //                if(apiResult != CY_BLE_SUCCESS)
        //                {
        //                    DBG_PRINTF("Cy_BLE_GAP_SetSecurityKeys API Error: 0x%x \r\n", apiResult);
        //                }
        //            }
        break;

    case CY_BLE_EVT_GAP_KEYS_GEN_COMPLETE:
        break;

    case CY_BLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE:
        break;

    case CY_BLE_EVT_GAP_DEVICE_DISCONNECTED:
        /* Put device to discoverable mode so that remote can find it. */

        //apiResult = Cy_BLE_GAPP_StartAdvertisement(CY_BLE_ADVERTISING_FAST, CY_BLE_PERIPHERAL_CONFIGURATION_0_INDEX);
        StartAdvertisement();

        break;

    case CY_BLE_EVT_GAP_AUTH_COMPLETE:
        break;

    case CY_BLE_EVT_GAP_ENCRYPT_CHANGE:
        //DBG_PRINTF("CY_BLE_EVT_GAP_ENCRYPT_CHANGE: %d \r\n", *(uint8_t *)eventParam);
        break;

    case CY_BLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT:
        //DBG_PRINTF("CY_BLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT \r\n");
        break;

    case CY_BLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
        //DBG_PRINTF("CY_BLE_EVT_GAPP_ADVERTISEMENT_START_STOP, state: %d \r\n", Cy_BLE_GetAdvertisementState());

        //            if(Cy_BLE_GetAdvertisementState() == CY_BLE_ADV_STATE_STOPPED)
        //            {
        //               /* Fast and slow advertising period complete, go to low-power
        //                 * mode (Hibernate) and wait for external
        //                 * user event to wake device up again */
        //               apiResult = Cy_BLE_GAPP_StartAdvertisement(CY_BLE_ADVERTISING_SLOW, CY_BLE_PERIPHERAL_CONFIGURATION_0_INDEX);
        //            }

        break;

        /**********************************************************
        *                       GATT Events
        ***********************************************************/

    case CY_BLE_EVT_GATT_CONNECT_IND:
    {
        connHandle = *(cy_stc_ble_conn_handle_t *)eventParam;

        gapDevice[ connHandle.attId ].connHandle.attId    = connHandle.attId;
        gapDevice[ connHandle.attId ].connHandle.bdHandle = connHandle.bdHandle;
        
        //appConnHandle = *(cy_stc_ble_conn_handle_t *)eventParam;
        //            DBG_PRINTF("CY_BLE_EVT_GATT_CONNECT_IND: %x, %x \r\n",
        //                (*(cy_stc_ble_conn_handle_t *)eventParam).attId,
        //                (*(cy_stc_ble_conn_handle_t *)eventParam).bdHandle);
    }
    break;

    case CY_BLE_EVT_GATT_DISCONNECT_IND:
        connHandle = *(cy_stc_ble_conn_handle_t *)eventParam;

        /* Update connection handle array */
        gapDevice[ connHandle.attId ].connHandle.attId    = connHandle.attId;
        gapDevice[ connHandle.attId ].connHandle.bdHandle = CY_BLE_INVALID_CONN_HANDLE_VALUE;
        gapDevice[ connHandle.attId ].faseSend   = 0;
        gapDevice[ connHandle.attId ].errCode    = 0;
        gapDevice[ connHandle.attId ].errCommand = 0;
        break;

    case CY_BLE_EVT_GATTS_WRITE_REQ:
        // SCrittura con risposta

        wrReqParam = (cy_stc_ble_gatts_write_cmd_req_param_t *)eventParam;
        
        // wrReqParam->connHandle <-- il device che mi sta parlando
        
        // Prelevo l'handle della caratteristica cy_ble_gatt_db_attr_handle_t
        if (wrReqParam->handleValPair.attrHandle == CY_BLE_SERVER_UART_RX_DATA_CHAR_HANDLE)
        {
            // All data
            errInfo.errorCode = writePar(wrReqParam->handleValPair.value.val, wrReqParam->handleValPair.value.len, wrReqParam->connHandle );
            
            if (errInfo.errorCode != CY_BLE_GATT_ERR_NONE)
            {
                errInfo.opCode = CY_BLE_GATT_WRITE_REQ;
                errInfo.attrHandle = wrReqParam->handleValPair.attrHandle;
                Cy_BLE_GATTS_SendErrorRsp(&wrReqParam->connHandle, &errInfo);
                break;
            }
        }
        if (wrReqParam->handleValPair.attrHandle == CY_BLE_SERVER_UART_TX_DATA_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE)
        {
            // All data start notifica
            gattErr = Cy_BLE_GATTS_WriteAttributeValuePeer(&wrReqParam->connHandle, &wrReqParam->handleValPair);
            if (gattErr != CY_BLE_GATT_ERR_NONE)
            {
                /* Send an Error Response */
                errInfo.opCode = CY_BLE_GATT_WRITE_REQ;
                errInfo.attrHandle = wrReqParam->handleValPair.attrHandle;
                errInfo.errorCode = gattErr;
                Cy_BLE_GATTS_SendErrorRsp(&wrReqParam->connHandle, &errInfo);
                break;
            }
            if (wrReqParam->handleValPair.value.val[0] & CY_BLE_CCCD_NOTIFICATION)
            {
                
                gattsDbInfo.connHandle = wrReqParam->connHandle;
                //gattsDbInfo.handleValuePair
                
                flag.notifica = 1;
            }
        }

        Cy_BLE_GATTS_WriteRsp(wrReqParam->connHandle);
        //Cy_BLE_GATTS_WriteRsp(appConnHandle);
        break;

    case CY_BLE_EVT_GATTS_WRITE_CMD_REQ:
        // Scrittura senza risposta
        wrReqParam = (cy_stc_ble_gatts_write_cmd_req_param_t *)eventParam;
        if (wrReqParam->handleValPair.attrHandle == CY_BLE_SERVER_UART_RX_DATA_CHAR_HANDLE)
        {
            errInfo.errorCode = writePar(wrReqParam->handleValPair.value.val, wrReqParam->handleValPair.value.len, wrReqParam->connHandle );
        }
        break;

    case CY_BLE_EVT_GATTS_XCNHG_MTU_REQ:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_XCNHG_MTU_REQ \r\n");
        break;

    case CY_BLE_EVT_GATTS_HANDLE_VALUE_CNF:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_HANDLE_VALUE_CNF \r\n");
        break;

    case CY_BLE_EVT_GATTS_PREP_WRITE_REQ:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_PREP_WRITE_REQ \r\n");
        break;

    case CY_BLE_EVT_GATTS_EXEC_WRITE_REQ:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_EXEC_WRITE_REQ \r\n");
        break;

        /**********************************************************
        *                  GATT Service Events 
        ***********************************************************/

    case CY_BLE_EVT_GATTS_INDICATION_ENABLED:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_INDICATION_ENABLED \r\n");
        break;

    case CY_BLE_EVT_GATTS_INDICATION_DISABLED:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_INDICATION_DISABLED \r\n");
        break;

    case CY_BLE_EVT_GATTC_INDICATION:
        //DBG_PRINTF("CY_BLE_EVT_GATTS_INDICATION \r\n");
        break;

    case CY_BLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
        /* Triggered on server side when client sends read request and when
            * characteristic has CY_BLE_GATT_DB_ATTR_CHAR_VAL_RD_EVENT property set.
            * This event could be ignored by application unless it need to response
            * by error response which needs to be set in gattErrorCode field of
            * event parameter. */
        //            DBG_PRINTF("CY_BLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ: handle: %x \r\n",
        //                        ((cy_stc_ble_gatts_char_val_read_req_t *)eventParam)->attrHandle);
        //rdReqParam = (cy_stc_ble_gatts_char_val_read_req_t*)eventParam;

        break;

        /**********************************************************
        *                       L2CAP Events 
        ***********************************************************/

    case CY_BLE_EVT_L2CAP_CONN_PARAM_UPDATE_REQ:
        //DBG_PRINTF("CY_BLE_EVT_L2CAP_CONN_PARAM_UPDATE_REQ \r\n");
        break;

        /**********************************************************
        *                       Other Events
        ***********************************************************/

    case CY_BLE_EVT_PENDING_FLASH_WRITE:
        /* Inform application that flash write is pending. Stack internal data 
            * structures are modified and require to be stored in Flash using 
            * Cy_BLE_StoreBondingData() */
        //DBG_PRINTF("CY_BLE_EVT_PENDING_FLASH_WRITE\r\n");

        break;

    default:
        //DBG_PRINTF("Other event: 0x%lx \r\n", event);
        break;
    }
}


    /* [] END OF FILE */