/*******************************************************************************
* File Name: main.c
*
* Version: 1.10
*
* Description:
* This code example demonstrates the PSoC 4 Voltage Comparator Component.
* It also uses an Analog Multiplexer Component for multiple inputs.
*
*******************************************************************************
* Copyright (2018-2020), Cypress Semiconductor Corporation. All rights reserved.
*******************************************************************************
* This software, including source code, documentation and related materials
* (“Software”), is owned by Cypress Semiconductor Corporation or one of its
* subsidiaries (“Cypress”) and is protected by and subject to worldwide patent
* protection (United States and foreign), United States copyright laws and
* international treaty provisions. Therefore, you may use this Software only
* as provided in the license agreement accompanying the software package from
* which you obtained this Software (“EULA”).
*
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
* non-transferable license to copy, modify, and compile the Software source
* code solely for use in connection with Cypress’s integrated circuit products.
* Any reproduction, modification, translation, compilation, or representation
* of this Software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
* reserves the right to make changes to the Software without notice. Cypress
* does not assume any liability arising out of the application or use of the
* Software or any product or circuit described in the Software. Cypress does
* not authorize its products for use in any products where a malfunction or
* failure of the Cypress product may reasonably be expected to result in
* significant property damage, injury or death (“High Risk Product”). By
* including Cypress’s product in a High Risk Product, the manufacturer of such
* system or application assumes all risk of such use and in doing so agrees to
* indemnify Cypress against all liability. 
*******************************************************************************/

#include <project.h>

#define MAX_CHANNELS 3

/* Turns off all LEDs */
#define LED_TURN_OFF    \
        do{             \
            Blue_LED_Write(1u); \
            Red_LED_Write(1u);  \
            Green_LED_Write(1u);\
        }while(0)

/*******************************************************************************
* Function Name: main
********************************************************************************
* Summary:
*  The main function performs the following actions:
*   1.  Initializes Voltage Comparator and Multiplexer Components 
*   2.	The Comparator output is captured in the Status Register.
*   3.  If the button is pressed and released, the Mux is changed to the next static divider voltage.
*   4.	The specified LED is controlled by the captured Comparator output.	
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
int main()
{
    /* Current mux channel */
    uint8 channel = 0;
    
    /* Copy of status_register Component */
    uint8 status;
    
    /* Initializes the Components */
    Comp_Start();
    Mux_Start();
    Mux_Select(channel);
    
    for(;;)
    {
        /* Debounce button */
        CyDelay(20/*msec*/);
        
        /* If button is pressed */
        if(button_Read() == 0)
        {
            /* Cycles through mux channels */
            channel = (channel + 1) % MAX_CHANNELS;
            Mux_Select(channel);
            
            /* Waits until button is not pushed */
            while(button_Read() == 0){}
        }
                
        /* Current channel corresponds to specific LED, 
           checks status register and controls LED */
        status = Status_Register_Read();
        LED_TURN_OFF;
        switch(channel)
        {
            case 0:
                Blue_LED_Write(status);
                break;
            case 1:
                Red_LED_Write(status);
                break;
            case 2:
                Green_LED_Write(status);
                break;
            default:
                /* Error */
                break;
        }
    }
}
