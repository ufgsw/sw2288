## EASY GATE / OPEN AIR     1.2.0

Serratura con controllo accessi per il cancelletto
Possibilità di apertura in diversi modi ( sfruttando il BLE )

* *Con uno smartphone*
* *Con una tastiera numerica*
* *Con un lettore di Tag mifire Classic*
* *Con un lettore di impronte*

---



### ADVERTISING PACKET 

|               |                  |                                     |       |
| ------------- | ---------------- | ----------------------------------- | ----- |
|               |                  | Value                               | Index |
| **AD Data 1** | length           | 0x02                                | 0     |
|               | **flags**        | 0x01                                | 1     |
|               |                  | 0x06                                | 2     |
| **AD Data 2** | length           | 0x011                               | 3     |
|               | **UUID**         | 0x06                                | 4     |
|               |                  | 0x31                                | 5     |
|               |                  | 0x01                                | 6     |
|               |                  | 0x9b                                | 7     |
|               |                  | 0x5f                                | 8     |
|               |                  | 0x80                                | 9     |
|               |                  | 0x00                                | 10    |
|               |                  | 0x00                                | 11    |
|               |                  | 0x80                                | 12    |
|               |                  | 0x00                                | 13    |
|               |                  | 0x10                                | 14    |
|               |                  | 0x00                                | 15    |
|               |                  | 0x00                                | 16    |
|               |                  | 0xd0                                | 17    |
|               |                  | 0xcd                                | 18    |
|               |                  | 0x03                                | 19    |
|               |                  | 0x00                                | 20    |
| **AD Data 3** | Length           | 0x09                                | 21    |
|               | **Service data** | 0x16                                | 22    |
|               |                  | --                                  | 23    |
|               |                  | --                                  | 24    |
|               |                  | --                                  | 25    |
|               |                  | --                                  | 26    |
|               |                  | --                                  | 27    |
|               | Tipo             | **2** *Easygate*<br>**3** *Openair* | 28    |
|               | Protocollo       |                                     | 29    |
|               |                  | --                                  | 30    |



---



#### Changelog

##### Release 1.2.1

* Risolto un Bug sulla lettura di diversi tag uno dietro l'altro ( buffer rx della seriale non azzerato )

##### Release 1.2.0

*  Viene tenuta valida solo una maschera per ogni utente ( viene deciso che cosa apre un utente : K1, K2, K3 indipendentemente da cosa ha utilizzato )

##### Release 1.1.9	

* Risolto un bug nel salvataggio dei nomi rele

##### Release 1.1.8

* Risolto il riconoscimento del fingerprint ( Montato o non montato )
* Non serve più cavallottare J11 con fingerprint assente

##### Release 1.1.7

* Risolto un bug nella versione senza il fingerprint

##### Release 1.1.6

* Inserito nell'advertising il tipo di scheda e il numero di protocollo nell'advertising

##### Release 1.1.4

* Corretti i permessi di apertura con tastierino ( abilitati tutti e tre i rele ) con un utente affittuario

##### Release 1.1.3

* Corretto un bug sul led lampeggiante durante l'accoppiamento con un telefono
* Aggiunto il comando **0x3b** per la lettura/settaggio dei nomi rele
* Tolti dai parametri inviati i nomi rele ( messaggio troppo lungo per il cloud )

##### Release 1.1.2

* Corretto un bug sul comando 0x43

##### Release 1.1.1

* Modificato il protocollo di invio e ricezione dei dati degli utenti

##### Release 1.1.0

* Aggiunto il comando **0x43** per l’apertura da smartphone con codice per affittuario

* Riportato il comando write/read Par a **0x01**

* Aggiunta la maschera di apertura nei log

* Aggiunto il tempo di ritardo nell’attivazione dei rele

* Tolto il campo ‘quando attiva’ dai parametri dei rele

* ***Cambiata la mappatura dei dati in FRam*** 

**Release 1.0.5**

* Risolto un Bug sulla trasmissione dei parametri

##### Release 1.0.3

* Modificata la maschera di apertura

##### Release 1.0.2

* Aggiunte le maschere di apertura nei parametri utente

* Aggiunti nei parametri I nomi delle 3 uscite
