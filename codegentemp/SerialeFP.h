/***************************************************************************//**
* \file SerialeFP.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(SerialeFP_CY_SCB_UART_PDL_H)
#define SerialeFP_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define SerialeFP_DIRECTION  (3U)
#define SerialeFP_ENABLE_RTS (0U)
#define SerialeFP_ENABLE_CTS (0U)

/* UART direction enum */
#define SerialeFP_RX    (0x1U)
#define SerialeFP_TX    (0x2U)

#define SerialeFP_ENABLE_RX  (0UL != (SerialeFP_DIRECTION & SerialeFP_RX))
#define SerialeFP_ENABLE_TX  (0UL != (SerialeFP_DIRECTION & SerialeFP_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void SerialeFP_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t SerialeFP_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void SerialeFP_DeInit(void);
__STATIC_INLINE void SerialeFP_Enable(void);
__STATIC_INLINE void SerialeFP_Disable(void);

/* Register callback. */
__STATIC_INLINE void SerialeFP_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (SerialeFP_ENABLE_CTS)
__STATIC_INLINE void SerialeFP_EnableCts(void);
__STATIC_INLINE void SerialeFP_DisableCts(void);
#endif /* (SerialeFP_ENABLE_CTS) */

#if (SerialeFP_ENABLE_RTS)
__STATIC_INLINE void     SerialeFP_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t SerialeFP_GetRtsFifoLevel(void);
#endif /* (SerialeFP_ENABLE_RTS) */

__STATIC_INLINE void SerialeFP_EnableSkipStart(void);
__STATIC_INLINE void SerialeFP_DisableSkipStart(void);

#if (SerialeFP_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t SerialeFP_Get(void);
__STATIC_INLINE uint32_t SerialeFP_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     SerialeFP_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t SerialeFP_GetRxFifoStatus(void);
__STATIC_INLINE void     SerialeFP_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t SerialeFP_GetNumInRxFifo(void);
__STATIC_INLINE void     SerialeFP_ClearRxFifo(void);
#endif /* (SerialeFP_ENABLE_RX) */

#if (SerialeFP_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t SerialeFP_Put(uint32_t data);
__STATIC_INLINE uint32_t SerialeFP_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     SerialeFP_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     SerialeFP_PutString(char_t const string[]);
__STATIC_INLINE void     SerialeFP_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t SerialeFP_GetTxFifoStatus(void);
__STATIC_INLINE void     SerialeFP_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t SerialeFP_GetNumInTxFifo(void);
__STATIC_INLINE bool     SerialeFP_IsTxComplete(void);
__STATIC_INLINE void     SerialeFP_ClearTxFifo(void);
#endif /* (SerialeFP_ENABLE_TX) */

#if (SerialeFP_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     SerialeFP_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     SerialeFP_StopRingBuffer(void);
__STATIC_INLINE void     SerialeFP_ClearRingBuffer(void);
__STATIC_INLINE uint32_t SerialeFP_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t SerialeFP_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     SerialeFP_AbortReceive(void);
__STATIC_INLINE uint32_t SerialeFP_GetReceiveStatus(void);
__STATIC_INLINE uint32_t SerialeFP_GetNumReceived(void);
#endif /* (SerialeFP_ENABLE_RX) */

#if (SerialeFP_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t SerialeFP_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     SerialeFP_AbortTransmit(void);
__STATIC_INLINE uint32_t SerialeFP_GetTransmitStatus(void);
__STATIC_INLINE uint32_t SerialeFP_GetNumLeftToTransmit(void);
#endif /* (SerialeFP_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void SerialeFP_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t SerialeFP_initVar;
extern cy_stc_scb_uart_config_t const SerialeFP_config;
extern cy_stc_scb_uart_context_t SerialeFP_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define SerialeFP_HW     ((CySCB_Type *) SerialeFP_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: SerialeFP_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t SerialeFP_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(SerialeFP_HW, config, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_DeInit(void)
{
    Cy_SCB_UART_DeInit(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_Enable(void)
{
    Cy_SCB_UART_Enable(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_Disable(void)
{
    Cy_SCB_UART_Disable(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(SerialeFP_HW, callback, &SerialeFP_context);
}


#if (SerialeFP_ENABLE_CTS)
/*******************************************************************************
* Function Name: SerialeFP_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(SerialeFP_HW);
}
#endif /* (SerialeFP_ENABLE_CTS) */


#if (SerialeFP_ENABLE_RTS)
/*******************************************************************************
* Function Name: SerialeFP_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(SerialeFP_HW, level);
}


/*******************************************************************************
* Function Name: SerialeFP_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(SerialeFP_HW);
}
#endif /* (SerialeFP_ENABLE_RTS) */


/*******************************************************************************
* Function Name: SerialeFP_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(SerialeFP_HW);
}


#if (SerialeFP_ENABLE_RX)
/*******************************************************************************
* Function Name: SerialeFP_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_Get(void)
{
    return Cy_SCB_UART_Get(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(SerialeFP_HW, buffer, size);
}


/*******************************************************************************
* Function Name: SerialeFP_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(SerialeFP_HW, buffer, size);
}


/*******************************************************************************
* Function Name: SerialeFP_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(SerialeFP_HW, clearMask);
}


/*******************************************************************************
* Function Name: SerialeFP_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(SerialeFP_HW);
}
#endif /* (SerialeFP_ENABLE_RX) */


#if (SerialeFP_ENABLE_TX)
/*******************************************************************************
* Function Name: SerialeFP_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(SerialeFP_HW,data);
}


/*******************************************************************************
* Function Name: SerialeFP_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(SerialeFP_HW, buffer, size);
}


/*******************************************************************************
* Function Name: SerialeFP_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(SerialeFP_HW, buffer, size);
}


/*******************************************************************************
* Function Name: SerialeFP_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(SerialeFP_HW, string);
}


/*******************************************************************************
* Function Name: SerialeFP_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(SerialeFP_HW, breakWidth);
}


/*******************************************************************************
* Function Name: SerialeFP_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(SerialeFP_HW, clearMask);
}


/*******************************************************************************
* Function Name: SerialeFP_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool SerialeFP_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(SerialeFP_HW);
}


/*******************************************************************************
* Function Name: SerialeFP_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(SerialeFP_HW);
}
#endif /* (SerialeFP_ENABLE_TX) */


#if (SerialeFP_ENABLE_RX)
/*******************************************************************************
* Function Name: SerialeFP_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(SerialeFP_HW, buffer, size, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t SerialeFP_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(SerialeFP_HW, buffer, size, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(SerialeFP_HW, &SerialeFP_context);
}
#endif /* (SerialeFP_ENABLE_RX) */


#if (SerialeFP_ENABLE_TX)
/*******************************************************************************
* Function Name: SerialeFP_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t SerialeFP_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(SerialeFP_HW, buffer, size, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(SerialeFP_HW, &SerialeFP_context);
}


/*******************************************************************************
* Function Name: SerialeFP_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t SerialeFP_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(SerialeFP_HW, &SerialeFP_context);
}
#endif /* (SerialeFP_ENABLE_TX) */


/*******************************************************************************
* Function Name: SerialeFP_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void SerialeFP_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(SerialeFP_HW, &SerialeFP_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* SerialeFP_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
