/***************************************************************************//**
* \file Seriale.h
* \version 2.0
*
*  This file provides constants and parameter values for the UART component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(Seriale_CY_SCB_UART_PDL_H)
#define Seriale_CY_SCB_UART_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_uart.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*   Initial Parameter Constants
****************************************/

#define Seriale_DIRECTION  (3U)
#define Seriale_ENABLE_RTS (0U)
#define Seriale_ENABLE_CTS (0U)

/* UART direction enum */
#define Seriale_RX    (0x1U)
#define Seriale_TX    (0x2U)

#define Seriale_ENABLE_RX  (0UL != (Seriale_DIRECTION & Seriale_RX))
#define Seriale_ENABLE_TX  (0UL != (Seriale_DIRECTION & Seriale_TX))


/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void Seriale_Start(void);

/* Basic functions */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale_Init(cy_stc_scb_uart_config_t const *config);
__STATIC_INLINE void Seriale_DeInit(void);
__STATIC_INLINE void Seriale_Enable(void);
__STATIC_INLINE void Seriale_Disable(void);

/* Register callback. */
__STATIC_INLINE void Seriale_RegisterCallback(cy_cb_scb_uart_handle_events_t callback);

/* Configuration change. */
#if (Seriale_ENABLE_CTS)
__STATIC_INLINE void Seriale_EnableCts(void);
__STATIC_INLINE void Seriale_DisableCts(void);
#endif /* (Seriale_ENABLE_CTS) */

#if (Seriale_ENABLE_RTS)
__STATIC_INLINE void     Seriale_SetRtsFifoLevel(uint32_t level);
__STATIC_INLINE uint32_t Seriale_GetRtsFifoLevel(void);
#endif /* (Seriale_ENABLE_RTS) */

__STATIC_INLINE void Seriale_EnableSkipStart(void);
__STATIC_INLINE void Seriale_DisableSkipStart(void);

#if (Seriale_ENABLE_RX)
/* Low level: Receive direction. */
__STATIC_INLINE uint32_t Seriale_Get(void);
__STATIC_INLINE uint32_t Seriale_GetArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale_GetArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t Seriale_GetRxFifoStatus(void);
__STATIC_INLINE void     Seriale_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Seriale_GetNumInRxFifo(void);
__STATIC_INLINE void     Seriale_ClearRxFifo(void);
#endif /* (Seriale_ENABLE_RX) */

#if (Seriale_ENABLE_TX)
/* Low level: Transmit direction. */
__STATIC_INLINE uint32_t Seriale_Put(uint32_t data);
__STATIC_INLINE uint32_t Seriale_PutArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale_PutArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale_PutString(char_t const string[]);
__STATIC_INLINE void     Seriale_SendBreakBlocking(uint32_t breakWidth);
__STATIC_INLINE uint32_t Seriale_GetTxFifoStatus(void);
__STATIC_INLINE void     Seriale_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Seriale_GetNumInTxFifo(void);
__STATIC_INLINE bool     Seriale_IsTxComplete(void);
__STATIC_INLINE void     Seriale_ClearTxFifo(void);
#endif /* (Seriale_ENABLE_TX) */

#if (Seriale_ENABLE_RX)
/* High level: Ring buffer functions. */
__STATIC_INLINE void     Seriale_StartRingBuffer(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale_StopRingBuffer(void);
__STATIC_INLINE void     Seriale_ClearRingBuffer(void);
__STATIC_INLINE uint32_t Seriale_GetNumInRingBuffer(void);

/* High level: Receive direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale_Receive(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale_AbortReceive(void);
__STATIC_INLINE uint32_t Seriale_GetReceiveStatus(void);
__STATIC_INLINE uint32_t Seriale_GetNumReceived(void);
#endif /* (Seriale_ENABLE_RX) */

#if (Seriale_ENABLE_TX)
/* High level: Transmit direction functions. */
__STATIC_INLINE cy_en_scb_uart_status_t Seriale_Transmit(void *buffer, uint32_t size);
__STATIC_INLINE void     Seriale_AbortTransmit(void);
__STATIC_INLINE uint32_t Seriale_GetTransmitStatus(void);
__STATIC_INLINE uint32_t Seriale_GetNumLeftToTransmit(void);
#endif /* (Seriale_ENABLE_TX) */

/* Interrupt handler */
__STATIC_INLINE void Seriale_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t Seriale_initVar;
extern cy_stc_scb_uart_config_t const Seriale_config;
extern cy_stc_scb_uart_context_t Seriale_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define Seriale_HW     ((CySCB_Type *) Seriale_SCB__HW)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: Seriale_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale_Init(cy_stc_scb_uart_config_t const *config)
{
   return Cy_SCB_UART_Init(Seriale_HW, config, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_DeInit(void)
{
    Cy_SCB_UART_DeInit(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_Enable(void)
{
    Cy_SCB_UART_Enable(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_Disable(void)
{
    Cy_SCB_UART_Disable(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_RegisterCallback(cy_cb_scb_uart_handle_events_t callback)
{
    Cy_SCB_UART_RegisterCallback(Seriale_HW, callback, &Seriale_context);
}


#if (Seriale_ENABLE_CTS)
/*******************************************************************************
* Function Name: Seriale_EnableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_EnableCts(void)
{
    Cy_SCB_UART_EnableCts(Seriale_HW);
}


/*******************************************************************************
* Function Name: Cy_SCB_UART_DisableCts
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableCts() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_DisableCts(void)
{
    Cy_SCB_UART_DisableCts(Seriale_HW);
}
#endif /* (Seriale_ENABLE_CTS) */


#if (Seriale_ENABLE_RTS)
/*******************************************************************************
* Function Name: Seriale_SetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_SetRtsFifoLevel(uint32_t level)
{
    Cy_SCB_UART_SetRtsFifoLevel(Seriale_HW, level);
}


/*******************************************************************************
* Function Name: Seriale_GetRtsFifoLevel
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRtsFifoLevel() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetRtsFifoLevel(void)
{
    return Cy_SCB_UART_GetRtsFifoLevel(Seriale_HW);
}
#endif /* (Seriale_ENABLE_RTS) */


/*******************************************************************************
* Function Name: Seriale_EnableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_EnableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_EnableSkipStart(void)
{
    Cy_SCB_UART_EnableSkipStart(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_DisableSkipStart
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_DisableSkipStart() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_DisableSkipStart(void)
{
    Cy_SCB_UART_DisableSkipStart(Seriale_HW);
}


#if (Seriale_ENABLE_RX)
/*******************************************************************************
* Function Name: Seriale_Get
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Get() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_Get(void)
{
    return Cy_SCB_UART_Get(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_GetArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_GetArray(Seriale_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale_GetArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_GetArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_GetArrayBlocking(Seriale_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetRxFifoStatus(void)
{
    return Cy_SCB_UART_GetRxFifoStatus(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearRxFifoStatus(Seriale_HW, clearMask);
}


/*******************************************************************************
* Function Name: Seriale_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetNumInRxFifo(void)
{
    return Cy_SCB_UART_GetNumInRxFifo(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_ClearRxFifo(void)
{
    Cy_SCB_UART_ClearRxFifo(Seriale_HW);
}
#endif /* (Seriale_ENABLE_RX) */


#if (Seriale_ENABLE_TX)
/*******************************************************************************
* Function Name: Seriale_Put
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Put() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_Put(uint32_t data)
{
    return Cy_SCB_UART_Put(Seriale_HW,data);
}


/*******************************************************************************
* Function Name: Seriale_PutArray
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_PutArray(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_PutArray(Seriale_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale_PutArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_PutArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_UART_PutArrayBlocking(Seriale_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Seriale_PutString
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_PutString() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_PutString(char_t const string[])
{
    Cy_SCB_UART_PutString(Seriale_HW, string);
}


/*******************************************************************************
* Function Name: Seriale_SendBreakBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_SendBreakBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_SendBreakBlocking(uint32_t breakWidth)
{
    Cy_SCB_UART_SendBreakBlocking(Seriale_HW, breakWidth);
}


/*******************************************************************************
* Function Name: Seriale_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetTxFifoStatus(void)
{
    return Cy_SCB_UART_GetTxFifoStatus(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_UART_ClearTxFifoStatus(Seriale_HW, clearMask);
}


/*******************************************************************************
* Function Name: Seriale_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetNumInTxFifo(void)
{
    return Cy_SCB_UART_GetNumInTxFifo(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool Seriale_IsTxComplete(void)
{
    return Cy_SCB_UART_IsTxComplete(Seriale_HW);
}


/*******************************************************************************
* Function Name: Seriale_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_ClearTxFifo(void)
{
    Cy_SCB_UART_ClearTxFifo(Seriale_HW);
}
#endif /* (Seriale_ENABLE_TX) */


#if (Seriale_ENABLE_RX)
/*******************************************************************************
* Function Name: Seriale_StartRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StartRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_StartRingBuffer(void *buffer, uint32_t size)
{
    Cy_SCB_UART_StartRingBuffer(Seriale_HW, buffer, size, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_StopRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_StopRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_StopRingBuffer(void)
{
    Cy_SCB_UART_StopRingBuffer(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_ClearRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_ClearRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_ClearRingBuffer(void)
{
    Cy_SCB_UART_ClearRingBuffer(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_GetNumInRingBuffer
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumInRingBuffer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetNumInRingBuffer(void)
{
    return Cy_SCB_UART_GetNumInRingBuffer(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_Receive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Receive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale_Receive(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Receive(Seriale_HW, buffer, size, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_GetReceiveStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetReceiveStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetReceiveStatus(void)
{
    return Cy_SCB_UART_GetReceiveStatus(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_AbortReceive
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortReceive() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_AbortReceive(void)
{
    Cy_SCB_UART_AbortReceive(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_GetNumReceived
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumReceived() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetNumReceived(void)
{
    return Cy_SCB_UART_GetNumReceived(Seriale_HW, &Seriale_context);
}
#endif /* (Seriale_ENABLE_RX) */


#if (Seriale_ENABLE_TX)
/*******************************************************************************
* Function Name: Seriale_Transmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Transmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_uart_status_t Seriale_Transmit(void *buffer, uint32_t size)
{
    return Cy_SCB_UART_Transmit(Seriale_HW, buffer, size, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_GetTransmitStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetTransmitStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetTransmitStatus(void)
{
    return Cy_SCB_UART_GetTransmitStatus(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_AbortTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_AbortTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_AbortTransmit(void)
{
    Cy_SCB_UART_AbortTransmit(Seriale_HW, &Seriale_context);
}


/*******************************************************************************
* Function Name: Seriale_GetNumLeftToTransmit
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_GetNumLeftToTransmit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Seriale_GetNumLeftToTransmit(void)
{
    return Cy_SCB_UART_GetNumLeftToTransmit(Seriale_HW, &Seriale_context);
}
#endif /* (Seriale_ENABLE_TX) */


/*******************************************************************************
* Function Name: Seriale_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_UART_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Seriale_Interrupt(void)
{
    Cy_SCB_UART_Interrupt(Seriale_HW, &Seriale_context);
}

#if defined(__cplusplus)
}
#endif

#endif /* Seriale_CY_SCB_UART_PDL_H */


/* [] END OF FILE */
